<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    
    public function location()
    {
            $ip = \Request::ip();
            $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
            //$reg = explode(" ",$details->region);
            echo "<pre>";
           // print_r(strtolower($reg[0]));
            print_r($details);
            exit;
    }




    public function index(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb, Request $request)
    {






        if(session('ip') != \Request::ip())
        {
             $today = date("y-m-d");
            //return session('ip');
            $ip = \Request::ip();
            $request->session()->put('ip', \Request::ip());
            $counter = DB::table('visitors')->first();
            
            $hit = $counter->counter;
            $daily = $counter->daily_count;
            $update_time = $counter->updated_at;

            $visit = DB::table('visitors')->first();
//var_dump($visit);exit;


             $location_details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
            
             $region = explode(" ",$location_details->region);
             $details = strtolower($region[0]);
            
            $newCount = DB::table('visitors')->update(
                [
                    'counter'       => $hit+1,
                    'daily_count' => $daily+1,
                    // 'updated_at'    => $date,

                ]);
            
            if ($update_time != $today) {
                //echo $update_time." ".$today;
                DB::table('visitors')->update(
                [
                    
                    'daily_count' => 1,
                    'updated_at'    => $today,

                ]);
            }
           // $details = $details_location->region;
            

            if($details == 'barisal')
            {
                DB::table('visitors')->update(
                [                 
                    'barisal' => $visit->barisal + 1,
                ]);
            }
            if($details == 'chittagong')
            {
                 DB::table('visitors')->update(
                [
                    'chittagong' => $visit->chittagong + 1,
                ]);

            }

            if($details == 'dhaka')
            {
                 DB::table('visitors')->update(
                [
                'dhaka' => $visit->dhaka + 1,
                ]);

            }

            if($details == 'khulna')
            {
                 DB::table('visitors')->update(
                [
                    'khulna' => $visit->khulna + 1,
                ]);
                
            }

            if($details == 'mymensingh')
            {
                 DB::table('visitors')->update(
                [
                    'mymensingh' => $visit->mymensingh + 1,
                ]);

            }

            if($details == 'rajshahi')
            {
                 DB::table('visitors')->update(
                [
                    'rajshahi' => $visit->rajshahi + 1,
                ]);
                
            }

            if($details == 'rangpur')
            {
                 DB::table('visitors')->update(
                [
                    'rangpur' => $visit->rangpur + 1,
                ]);
                
            }

            if($details == 'sylhet')
            {
                 DB::table('visitors')->update(
                [
                    'sylhet' => $visit->sylhet + 1,

                ]);
                
            }



        }
        











        //  app id 1416923468572764

        $users = DB::table('users')->get();
        $offers = DB::table('pazzles')->get();
//var_dump($offers);exit;
        $all_orders = DB::table('orders')
            ->get();
        $upcoming_orders = DB::table('orders')
            ->where('order_date','>=',date('d-m-Y'))
            ->get();
        $pending_orders = DB::table('orders')
            ->where('publication_status',0)
            //->distinct(['order_number'])
            ->distinct()
            ->get(['order_date']);
            //return $pending_orders;
        $upcoming_pending_orders = DB::table('orders')
            ->where('order_date','>=',date('d-m-Y'))
            ->where('publication_status',0)
            ->get();
            //return $upcoming_pending_orders;


        $products = DB::table('products')
            ->where('publication_status',1)
            ->get();
            //return $upcoming_pending_orders;
        $contacts = DB::table('contacts')
            ->get();
            //return $upcoming_pending_orders;



         $dates = DB::table('orders')->distinct()
                  ->orderby('order_date','asc')->get(['order_date']);

          $dateArray  = array();
          $countArray = array();
          $i=0;

          foreach ($pending_orders as $date) {
            if ($i>9) {
                break;
            }
              $dateArray[] = $date->order_date;
                $date_count = DB::table('orders')
                ->where('order_date', $date->order_date )
                ->where('publication_status',0)
                ->distinct()->count('order_number');
                $countArray[] = $date_count;
                $i++;
          }
          //return $countArray;
// lineChartTest

            $product_chart = app()->chartjs
                     ->name('barChartTest')
                     ->type('line')
                     ->size(['width' => 100, 'height' => 45])
                     ->labels($dateArray)
                     ->datasets([
                         [
                             "label" => "Orders per day",
                             'backgroundColor' => 'skyblue',
                             'data' => $countArray
                         ]

                   
                     ])
                     ->options([]);


        
// visitor donat chart
                   
            $visitor = \DB::table('visitors')->first();
//print_r($visitor);
            $visitorArray = array();
            
            $visitorArray[] = $visitor->barisal;
            $visitorArray[] = $visitor->chittagong;
            $visitorArray[] = $visitor->dhaka;
            $visitorArray[] = $visitor->khulna;
            $visitorArray[] = $visitor->mymensingh;
            $visitorArray[] = $visitor->rajshahi;
            $visitorArray[] = $visitor->rangpur;
            $visitorArray[] = $visitor->sylhet;
//print_r($visitorArray);
//exit;
            $visitor_chart = app()->chartjs
                    ->name('pieChartTest')
                    ->type('doughnut')
                    ->size(['width' => 400, 'height' => 400])
                    ->labels(['Barisal','Chittagong','Dhaka','Khulna','Mymensingh','Rajshahi','Rangpur', 'Sylhet'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#f7970e', '#00b39a','#ff6664','#4caf50','#03a9f4','#111317','#673ab7','#ff5722','purple'],
                            'hoverBackgroundColor' => ['#FF6384', '#36A2EB', '#36A2EB', '#36A2EB', '#36A2EB', '#36A2EB', '#36A2EB', '#36A2EB'],
                            'data' => $visitorArray
                        ]
                    ])
                    ->options([
                         'legend'=> [
                                'display'=> false,
                                'position'=> 'left',
                                'labels' => [
                                    'fontColor' => 'teal'
                                    ]
                                ]
                        ]);
        
        
       
              
        $app_token = '1923089044631336|kmF7Hc5nRadVcCpC2K0ikF2U7LA';
        $fullresponse = $fb->get('/334422027017070/feed?fields=from,message,updated_time,comments,shares,reactions&limit=10', $app_token)->getDecodedBody();
        $response = $fullresponse['data'];
        
       


        return view('backend.dashboard',compact('product_chart','visitor_chart','response','users','products','pending_orders','visitor','contacts','offers'));
    }

    public function create()
    {
        return view('backend.add_admin');
    }


    public function subscribers()
    {
        $subscribers = DB::table('subscribes')->get();
        return view('backend.subscribers',compact('subscribers'));
    }

    public function deletesubscriber($id)
    {
        $subscribers = DB::table('subscribes')->where('id',$id)->delete();
        session::flash('message','Email Delete Successfully');
        return redirect()->back();
    }
    public function customers()
    {
        $customers = DB::table('customers')->get();
        return view('backend.customers',compact('customers'));
    }

    public function deletecustomer($id)
    {
        DB::table('customers')->where('id',$id)->delete();
        session::flash('message','Customer Delete Successfully');
        return redirect()->back();
    }
}
