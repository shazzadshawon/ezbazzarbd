<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class VideoController extends Controller
{
    public function edit_video()
    {
        $video = DB::table('video')->first();
        //return $video->video_name;
        return view('backend.video.editvideo',compact('video'));
    }


    public function update_video()
    {

        $input = Input::all();
        $video = DB::table('video')->first();
        $name = $video->video_name;

        unlink('public/uploads/video/'.$name);

        $file = array_get($input,'video_name');
        // SET UPLOAD PATH
        $destinationPath = 'public/uploads/video/';
        // GET THE FILE EXTENSION
        $extension = $file->getClientOriginalExtension();
        // RENAME THE UPLOAD WITH RANDOM NUMBER
        $fileName = rand(11111, 99999) . '.' . $extension;
        // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
        $upload_success = $file->move($destinationPath, $fileName);
//return $fileName;
        DB::table('video')
            ->where('id',1)
            ->update([
                'video_name' => $fileName,
            ]);




        return redirect()->back()->with('success','video updated Successfully');

    }

}
