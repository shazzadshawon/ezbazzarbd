<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Category;
use App\SubCategory;
use App\SliderImage;
use App\SubSubCategory;
use App\Product;
use App\Customer;
use App\Wishlist;
use DB;
class ViewContoller extends Controller
{



    public function index() {

        $main_categories = DB::table('categories')->get();
        $mega_offer = DB::table('pazzles')->where('mega_offer',1)->get();
        //return $mega_offer;
        $sliders = DB::table('slider_images')->get();
        $featured = DB::table('products')->where('offer_status',1)->where('publication_status',1)->get();
        $topsale = DB::table('products')->where('offer_status',2)->where('publication_status',1)->get();
        $newarrival = DB::table('products')->where('offer_status',3)->where('publication_status',1)->get();
        // $image = DB::table('product_images')->where('product_id',4)->get();
        // print_r($image);
        // exit();
                   //return 213323;
        return view('frontend.ezbazzar.main',compact('sliders','main_categories','mega_offer','featured','topsale','newarrival'));
    }

    public function ProductPage($id) {
         $subCategory = SubCategory::where('sub_category_id',$id)->first();
         $products = Product::where('sub_category_id', $id)
         ->where('publication_status',1)
                  ->orderBy('id', 'desc')
                 ->paginate(15);
        return view('frontend.ezbazzar.category')->with('products',$products)->with('SubCategories',$subCategory);
    }

 public function cart() {

        return view('frontend.ezbazzar.cart');
    }

    public function ProductPageManin($id) {
         $subCategory = Category::where('category_id',$id)->first();
         $products = Product::where('category_id', $id)
         ->where('publication_status',1)
                 ->orderBy('id', 'desc')
                 ->get();
        return view('main_category_product')->with('products',$products)->with('SubCategories',$subCategory);
    }

    public function ProductPageSub($id) {
         $subCategory = SubSubCategory::where('id',$id)->first();
         $products = Product::where('sub_sub_category_id', $id)
         ->where('publication_status',1)
                 ->orderBy('id', 'desc')
                 ->get();
        return view('sub_category_product')->with('products',$products)->with('SubCategories',$subCategory);
    }

    public function CrazyDeal($id) {
     
         $products = Product::where('offer_status', $id)
         ->where('publication_status',1)
                 ->orderBy('id', 'desc')
                 ->get();
        return view('crazy_deal')->with('products',$products)->with('id',$id);
    }


    public function OfferProduct($id) {
     
         $products = Product::where('offer_status', $id)
         ->where('publication_status',1)
                 ->orderBy('id', 'desc')
                 ->get();
        return view('offer_product')->with('products',$products)->with('id',$id);
    }
    


    public function SingleProductPage($id) {
     
         $productInfo = DB::table('products')->where('id',$id)->first();
         $categories = DB::table('categories')->get();
                
        return view('frontend.ezbazzar.single-product',compact('productInfo','categories'));
    }
    
    public function CustomerLogin() {
        
        return view('frontend.ezbazzar.authentication');
    }
     public function CustomerSignUp() {
        
        return view('registration');
    }
    public function about_us() {
        
        return view('frontend.ezbazzar.about');
    }
    public function contact_us() {
        
        return view('frontend.ezbazzar.contact');
    }

     public function md_message() {
        
        return view('md_message');
    }

    public function treamCondition() {
        
        return view('delivery_policy');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
           'customer_name'=>'required|max:255'
       ));
        $cuntomer = new Customer;
      
       $cuntomer->customer_name = $request->customer_name;
       $cuntomer->phone_number = $request->phone_number;
       $cuntomer->address = $request->address;
       $cuntomer->email_address = $request->email_address;
       $cuntomer->password = md5($request->password);

       if($cuntomer->save()){
              $request->Session()->put('customer_name',$cuntomer->customer_name);
              $request->Session()->put('customer_id',$cuntomer->id);
           Session::flash('message','Registration Completed ....!');
        return Redirect::to('/shipping');
       }else{
        Session::flash('message','Invalid info ....!');
        return Redirect::to('/Login-Customer');
       }
    }
public function CustomerLoginCheck(Request $request) {      

        //echo "login";
        //return view('admin.admin_master');
        $email_address = $request->email_address;

        $password = md5($request->password);
                
      
 
        $result = DB::table('customers')
                ->where('email_address', $email_address)    
                ->where('password', $password)
                ->first();
        
      
        
        if ($result) {
            //return view('admin.admin_master');
           $request->Session()->put('customer_name',$result->customer_name);
              $request->Session()->put('customer_id',$result->id);
              Session::flash('message','You are successfully  logged in!');
             return Redirect::to('/shipping');
        } else {
            Session::flash('message','User Id / Password Invalid');
            return Redirect::to('/User-Register');
        }
   
        
    }
    
       public function logoutcustomer()
    {
           Session::put('customer_name',null);
            Session::put('customer_id',null);

               Session::flash('message','You are successfully  Logged Out!');
       return Redirect::to('/User-Register');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function post_contact(Request $request)
    {
        //return $request->all();

        DB::table('contacts')->insert(
            [
                'contact_title' => $request->get('contact_title'),
                'contact_email' => $request->get('contact_email'),
                'contact_reference' => $request->get('contact_reference'),
                'contact_description' => $request->get('contact_description'),
                'contact_title' => $request->get('contact_title'),
            ]);

        return redirect()->back();


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function product_search(Request $request)
    {
       // return Input::all();
        //return $request->all();
        $cat_id = $request->get('cat');
        $query = $request->get('query');
        if ($cat_id==0) {
            $products = DB::table('products')->where('product_name', 'LIKE', '%'.$query.'%')->get();
        }
        else{
            $products = DB::table('products')
            ->where('category_id',$cat_id)
            ->where('product_name', 'LIKE', '%'.$query.'%')->get();
        }
        return view('product_search',compact('products'));
    }

}
