-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2017 at 11:04 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ezbazar`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_to_carts`
--

CREATE TABLE `add_to_carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_name_bn` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_code` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_price` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_quantity` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `add_to_carts`
--

INSERT INTO `add_to_carts` (`id`, `product_id`, `product_name`, `product_name_bn`, `product_code`, `product_price`, `product_quantity`, `session_id`, `size`, `created_at`, `updated_at`) VALUES
(27, 73, 'Polo', 'পোলো টি শার্ট ', 'PL515646', '141', '3', 'w4YX87HhnrMfspQwMLBBy0j2Ex3miRL0WiDfNyJo', 'None', '2017-08-31 02:19:32', '2017-08-31 02:31:55'),
(31, 72, 'Polo T-shirt', 'পোলো টি শার্ট ', 'PT654156', '640', '1', 'BF9TdrBiK7JCODLENduy9MeTvxbEluKeyahWXp0x', 'L', '2017-09-04 22:44:30', '2017-09-04 22:44:30');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `admin_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_name_bn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `mega_menu` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_name_bn`, `publication_status`, `mega_menu`, `created_at`, `updated_at`) VALUES
(33, 'MEN’S FASHION 1', '', 1, 1, '2017-06-07 03:00:56', '2017-08-27 05:05:15'),
(34, 'WOMEN’S FASHION', 'ওমেন্স ফ্যাশন', 1, 1, '2017-06-07 03:15:25', '2017-08-07 17:47:04'),
(35, 'HOME & KITCHEN', 'হোম এন্ড  কিচেন ', 1, 0, '2017-06-07 03:20:19', '2017-07-27 10:28:48'),
(37, 'ELECTRONICS', 'ইলেক্ট্রনিক্স প্রোডাক্ট', 1, 0, '2017-06-07 03:23:21', '2017-07-27 10:29:44'),
(38, 'HEALTH & BEAUTY', 'হেলথ এন্ড বিউটি ', 1, 0, '2017-06-07 03:27:25', '2017-07-27 10:31:17'),
(42, 'SHOES', 'SHOES', 1, NULL, '2017-07-26 10:42:07', '2017-07-27 11:25:10'),
(43, 'ACCESSORIES', 'ACCESSORIES', 1, NULL, '2017-07-26 10:42:37', '2017-07-26 10:42:37'),
(47, 'dehgh', 'dehgh', 1, NULL, '2017-09-05 03:02:29', '2017-09-05 03:02:29'),
(48, 'gfgf', 'gfgf', 1, NULL, '2017-09-05 03:02:32', '2017-09-05 03:02:32'),
(49, 'gfgfghh', 'gfgfghh', 1, NULL, '2017-09-05 03:02:36', '2017-09-05 03:02:36'),
(50, 'ukuki', 'ukuki', 1, NULL, '2017-09-05 03:02:40', '2017-09-05 03:02:40');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `contact_title` longtext COLLATE utf8_unicode_ci,
  `contact_email` longtext COLLATE utf8_unicode_ci,
  `contact_reference` longtext COLLATE utf8_unicode_ci,
  `contact_description` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `contact_title`, `contact_email`, `contact_reference`, `contact_description`, `created_at`, `updated_at`) VALUES
(1, 'Test', '01918278373', '01918278373', NULL, '2017-07-22 15:08:30', '2017-07-22 15:08:30'),
(2, 'Webmaster', 'qqq@rr.com', 'ewdwd', NULL, '2017-07-25 12:06:49', '2017-07-25 12:06:49'),
(3, 'ezbazzar 1', 'demo@ezbazzar.com', NULL, 'Ezbazzar', '2017-09-05 05:29:19', '2017-09-05 05:29:19');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `customer_name`, `phone_number`, `address`, `email_address`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Mahmud Hira', '01675646860', 'Dhaka', 'mahmud@gmail.com', '69eee85465438d9c013b14bb7210cc7d', '2017-01-09 03:50:23', '2017-01-09 03:50:23'),
(3, 'Sabbir', '01675646860', 'Dhaka', 'sabbir@gmail.com', '69eee85465438d9c013b14bb7210cc7d', '2017-01-09 03:54:18', '2017-01-09 03:54:18'),
(5, 'Mahmud Hira', '01675646860', 'Dhaka', 'mahmud@gmail.com', '69eee85465438d9c013b14bb7210cc7d', '2017-06-19 00:59:33', '2017-06-19 00:59:33'),
(6, 'VirgilFaw', '85916841844', 'http://www.0daymusic.org ', 'serverftp2017@mail.ru', '50a771b04f6427a5007bed0f77575f8a', '2017-06-30 06:38:02', '2017-06-30 06:38:02'),
(7, 'Mahmud', '01675646860', 'sdfgdsfsdf', 'info@kenakatazone.com', '69eee85465438d9c013b14bb7210cc7d', '2017-07-02 04:14:20', '2017-07-02 04:14:20'),
(8, 'anjuman', '01678504914', 'gaahhgghhjhhg', 'info@kenakatazone.com', '69eee85465438d9c013b14bb7210cc7d', '2017-07-02 05:14:52', '2017-07-02 05:14:52'),
(9, 'gffdd', '', '', 'info@kenakatazone.com', '69eee85465438d9c013b14bb7210cc7d', '2017-07-02 07:01:35', '2017-07-02 07:01:35'),
(10, 'Akash', '01348794433', 'Dhaka', 'akash@gmail.com', '94754d0abb89e4cf0a7f1c494dbb9d2c', '2017-07-24 03:18:37', '2017-07-24 03:18:37'),
(11, 'agv@g', '', '', 'akash@gmail.com', '94754d0abb89e4cf0a7f1c494dbb9d2c', '2017-07-26 02:28:26', '2017-07-26 02:28:26'),
(12, 'Agv', '123456', 'Banani, dhaka', 'agv@gmail.com', 'bda8cb4eeea1f95c458da6692128d915', '2017-07-26 02:29:54', '2017-07-26 02:29:54'),
(13, 'rony', '0192443659', 'mirpur-12, dhaka ', 'rony007@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '2017-08-10 04:25:10', '2017-08-10 04:25:10'),
(14, 'ezbazzar-user', '0133568', 'house #33, Road #14, DIT project, MerulBadda Dhaka, Bangladesh 1212', 'user@ezbazzarbd.com', 'e10adc3949ba59abbe56e057f20f883e', '2017-08-31 03:45:03', '2017-08-31 03:45:03'),
(15, 'akash', '123455', 'dhaka', 'akash@ezbazar.com', 'e10adc3949ba59abbe56e057f20f883e', '2017-08-31 04:02:04', '2017-08-31 04:02:04'),
(16, 'demo', '123456', 'dhaka', 'demo@ezbazzar.com', 'e10adc3949ba59abbe56e057f20f883e', '2017-09-04 21:34:05', '2017-09-04 21:34:05');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_01_01_124411_create_admins_table', 1),
(4, '2017_01_01_135930_create_categories_table', 2),
(5, '2017_01_02_085340_create_sub_categories_table', 3),
(6, '2017_01_02_133054_create_slider_images_table', 4),
(7, '2017_01_03_094319_create_product_images_table', 5),
(8, '2017_01_03_100037_create_products_table', 5),
(9, '2017_01_09_072750_create_customers_table', 6),
(10, '2017_01_09_123439_create_wishlists_table', 7),
(11, '2017_01_10_133258_create_add_to_carts_table', 8),
(12, '2017_01_11_114149_create_orders_table', 9),
(13, '2017_01_11_114233_create_shipping_addresses_table', 9),
(14, '2017_05_04_081139_create_sub_sub_categories_table', 10),
(15, '2017_05_08_080323_create_pazzles_table', 11),
(16, '2017_05_15_103110_create_subscribes_table', 12),
(17, '2017_06_18_063013_create_product_sizes_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_code` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_price` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_quantity` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `order_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `order_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `product_id`, `customer_id`, `product_name`, `product_code`, `product_price`, `product_quantity`, `size`, `order_number`, `session_id`, `publication_status`, `created_at`, `order_date`, `updated_at`) VALUES
(31, 78, 15, 'Shirt', 'shg664543', '520', '2', 'S', '19279', 'VIQM7Kcd9ZW4PEcYfsrjgjmVx72fuvzqpLyIozAk', 0, '2017-08-31 05:17:00', '31-08-2017', '2017-08-31 05:17:00'),
(32, 78, 15, 'Shirt', 'shg664543', '520', '1', 'M', '19279', 'VIQM7Kcd9ZW4PEcYfsrjgjmVx72fuvzqpLyIozAk', 0, '2017-08-31 05:17:00', '31-08-2017', '2017-08-31 05:17:00'),
(33, 80, 15, 'Panjabis & Sherwanis', 'PS564196', '5000', '1', 'None', '19279', 'VIQM7Kcd9ZW4PEcYfsrjgjmVx72fuvzqpLyIozAk', 0, '2017-08-31 05:17:01', '31-08-2017', '2017-08-31 05:17:01'),
(34, 71, 16, 'T-Shirtt', 'SF54145198', '600', '2', 'L', '18139', 'BF9TdrBiK7JCODLENduy9MeTvxbEluKeyahWXp0x', 0, '2017-09-04 21:36:17', '05-09-2017', '2017-09-04 21:36:17'),
(35, 88, 16, 'Kamiz', 'w238972893', '490', '1', 'None', '18139', 'BF9TdrBiK7JCODLENduy9MeTvxbEluKeyahWXp0x', 0, '2017-09-04 21:36:17', '05-09-2017', '2017-09-04 21:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pazzles`
--

CREATE TABLE `pazzles` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pazzle` int(11) NOT NULL,
  `pazzle_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `mega_offer` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pazzles`
--

INSERT INTO `pazzles` (`id`, `heading`, `pazzle`, `pazzle_image`, `publication_status`, `mega_offer`, `created_at`, `updated_at`) VALUES
(2, NULL, 34, '868.png', 1, NULL, '2017-09-04 21:14:44', '2017-09-04 21:14:44'),
(3, NULL, 35, '868.png', 1, NULL, '2017-09-04 21:15:00', '2017-09-04 21:15:00'),
(4, NULL, 36, 'testimonials.jpg', 1, NULL, '2017-09-04 21:15:24', '2017-09-04 21:15:24'),
(5, NULL, 34, '8903415526399_thumb_1.png', 1, NULL, '2017-09-04 21:15:53', '2017-09-04 21:15:53');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `sub_sub_category_id` int(11) DEFAULT NULL,
  `product_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_name_bn` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_code` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_price` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_quantity` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `discount` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description_bn` longtext COLLATE utf8_unicode_ci NOT NULL,
  `offer_status` tinyint(4) NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `sub_category_id`, `sub_sub_category_id`, `product_name`, `product_name_bn`, `product_code`, `product_price`, `product_quantity`, `discount`, `description`, `description_bn`, `offer_status`, `publication_status`, `created_at`, `updated_at`) VALUES
(71, 33, 34, 9, 'T-Shirtt', '', 'SF54145198', '600', '19', '', '<p>jdhs ushdb fusdbhf bsdbhf bshdbfhbasdhb fsdfhbdsbfhb sbdfbdsuf</p>\r\n', '', 1, 1, '2017-06-08 03:16:54', '2017-08-24 01:47:14'),
(72, 33, 34, 10, 'Polo T-shirt', 'পোলো টি শার্ট ', 'PT654156', '800', '16', '20', 'dsfh bhsbdf sdabn fbsdh bhsdhf bsdhf dfhdsjf ds b jjj', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">পোলো টি শার্ট &nbsp;&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;</span></font>', 1, 1, '2017-06-08 03:23:41', '2017-08-23 22:01:24'),
(73, 33, 34, 10, 'Polo', 'পোলো টি শার্ট ', 'PL515646', '150', '50', '6', 'hsdf bhsdbh bbshdh gsbd sdifn<br>dskj fnsdj sdnf adsifnisdnfi', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;পোলো টি শার্ট&nbsp;</span></font>', 1, 1, '2017-06-08 03:25:50', '2017-06-08 03:25:50'),
(74, 33, 34, 13, 'Pant', 'প্যান্ট', 'PT65465', '2000', '20', '3', 'fdh shg uidhfsig hdsfg dsfgdf<br>sdjf sif &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;drgjifdj gdfgdf', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">প্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটিপ্যান্ট এক্সপোর্ট কোয়ালিটি</span></font>', 1, 1, '2017-06-08 03:52:06', '2017-06-08 03:52:06'),
(75, 33, 34, 11, 'Shirt', 'শার্ট', 'sh6546541', '1550', '30', '', 'hjhfd hbdsfh hdsbfuhvbs hbdhsbsd<br>SD fjdsfi fipusd jsdof d\'s<br>dkjs fnsdufnguosd', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">&nbsp;&nbsp;</span></font><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">শার্টশার্টশার্টশার্টশার্টশার্টশার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্টশার্টশার্টশার্ট</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">শার্টশার্টশার্টশার্টশার্টশার্টশার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্টশার্টশার্টশার্ট &nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">শার্টশার্টশার্টশার্টশার্টশার্টশার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্টশার্টশার্টশার্ট</span>', 1, 1, '2017-06-10 21:44:06', '2017-06-10 21:44:06'),
(76, 33, 34, 14, 'Jeans', 'জিন্স ', 'JE6549', '1500', '19', '', 'hsbdf hsdb sdb fbsdflbsdfi adhb jcnv bjhdcbugdfgbsdf lsdfi sdfsdf sal bfsd\\as\\]as<div>asd</div><div>as</div><div>dasdf</div>', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">জিন্স &nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্</span></font><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;জিন্স&nbsp;</span></font></div>', 2, 1, '2017-06-11 02:43:27', '2017-08-23 22:01:24'),
(77, 33, 34, 9, 'T-shirt', 'টি -শার্ট  ', 'TS65165', '600', '60', '6', 'kjdsbn j sdhf ds dsg dg sd sd sdf sd fsd fs]dsdfds\\sd sd fsd<div>&nbsp;sdf sdfgds</div>', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;</span></font><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">টি -শার্ট &nbsp;টি -শার্ট &nbsp;</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;টি -শার্ট &nbsp;</span></font></div>', 2, 1, '2017-06-11 02:59:06', '2017-06-11 02:59:06'),
(78, 33, 34, 11, 'Shirt', 'শার্ট ', 'shg664543', '650', '50', '20', 'dshf sdhbsd blsdbfvh sdbhbvhds bhbsdh vshdbv dsbfv dsbhfvb hsdbvfh sbdhfv dsb', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;</span></font><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;শার্ট&nbsp;</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div>', 3, 1, '2017-06-11 03:17:36', '2017-06-11 03:17:36'),
(79, 33, 34, 10, 'Polo\'s', 'পোলো ', 'PL56dsfg', '1500', '20', '6', 'polo kajsdfn sbd hdsfg sadg dhghdfg', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;</span><br><span style=\"font-size: 13.3333px;\">পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;পোলো&nbsp;</span></font>', 3, 1, '2017-06-12 00:07:47', '2017-06-12 00:07:47'),
(80, 33, 35, 16, 'Panjabis & Sherwanis', 'পাঞ্জাবি ও শেরওয়ানি', 'PS564196', '5000', '20', '', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Panjabis &amp; Sherwanis</span></font>', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">&nbsp;</span></font><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span><span style=\"font-family: Arial, Verdana; font-size: 13.3333px;\">পাঞ্জাবি ও শেরওয়ানি&nbsp;</span>', 3, 1, '2017-06-13 00:04:52', '2017-06-13 00:04:52'),
(88, 34, 44, 0, 'Kamiz', 'null', 'w238972893', '500', '20', '2', 'Demo desription', 'null', 1, 1, '2017-07-25 02:51:11', '2017-07-25 02:51:11'),
(91, 42, 0, 0, 'Derma Seta', 'null', '0100', '2880', '1+', '', '<span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">Small and Fits on Your Countertop: Wouldn’t it be nice to not have to make those embarrassing trips to the salon only to have them give you painful and expensive waxing treatments? Waxing, Laser, Electric and other hair removal systems take up a ton of space and are embarrassing to have out in your house. Stop those embarrassing spa trips and awkward situations when you cannot find a place to store your skin care systems because now you can remove hair painlessly and easily in your own home with dermaSeta.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">No More Wasting Time in Your Bathroom Shaving:It not only makes a great painless alternative to waxing or laser hair removal treatments, but it is also an amazing and time saving option to having to shave your legs, arms and even unwanted facial hair.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">Shaving your legs becomes more of a chore and takes a ton of time to finish. You also usually end up missing small patches which can be extremely embarrassing, not to mention you have to start over again with shaving creams, razors, washing, etc... What’s even worse is that many of us end up with bumps, nicks and cuts which not only sting but can bleed and make our legs and skin look like a mess. Since the It is painless hair removal system does not use sharp and dangerous razors, you can consider cuts and scrapes from shaving a thing of the past! Not only will you never have to endure embarrassing cuts from shaving again, but because of the 550 rotations per minute and our derma crystal Pads, the derma Seta removes hair almost instantly saving you time to do other things besides sitting in a bathroom and shaving your arms and legs.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">We all could use extra time to spend with our families or even just on ourselves; so why not let derma Seta save you that extra time so you can enjoy your life and not spend it stuck in a bathroom shaving.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">Terms of Conditions</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">1)Free Home Delivery Service.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">2)No Delivery Charge.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">3)Outside Of Dhaka Delivery From SA Poribohon.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">4)Stock is Available.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">5)100% Genuine Products.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">6)Faster Delivery Service.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">Aire Bra 3in1</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">Aire Bra 3in1</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">Tk.2880</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">3in1 Shaver &amp; Epilater</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">3in1 Shaver &amp; Epilater</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">Benifits</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">No More Painful Waxing: Nobody likes to have hot wax poured on them and everyone cringes when it is time to pull the painful waxing paper off. Never again will you have to endure painful waxing treatments because this derma will not only provide you with the same results, but will also help you save a ton of money compared to expensive spa visits and trips to the doctor for laser hair removal treatments.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">Terms of Conditions</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">1)Free Home Delivery Service.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">2)No Delivery Charge.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">3)Outside Of Dhaka Delivery From SA Poribahon.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">4)Stock is Available.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">5)100% Genuine Products.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">6)Faster Delivery Service.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">7)Hotline:01611 39 22 22</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">8) www.lakespoint.net</span>', 'null', 1, 1, '2017-07-27 09:16:24', '2017-07-27 09:16:24'),
(93, 38, 61, 0, 'Air Bra', '', '0120', '2490', '1+', '', '<div><font color=\"#9d9d9d\" face=\"Roboto, Helvetica Neue, Helvetica, Arial, sans-serif\"><span style=\"font-size: 14px;\">{Éter|Son|Parecido} Bra In A LP {Atmosphere|Skies|Heavens} Shop</span></font></div><div><font color=\"#9d9d9d\" face=\"Roboto, Helvetica Neue, Helvetica, Arial, sans-serif\"><span style=\"font-size: 14px;\">Air bra {is among the most|is considered the most} preferred Bra by women worldwide.</span></font></div><div><font color=\"#9d9d9d\" face=\"Roboto, Helvetica Neue, Helvetica, Arial, sans-serif\"><span style=\"font-size: 14px;\">Air bra provides you full coverage of cleavage.</span></font></div><div><font color=\"#9d9d9d\" face=\"Roboto, Helvetica Neue, Helvetica, Arial, sans-serif\"><span style=\"font-size: 14px;\">Air bra {provides|offers} proper {condition}, postures and comfort.</span></font></div><div><font color=\"#9d9d9d\" face=\"Roboto, Helvetica Neue, Helvetica, Arial, sans-serif\"><span style=\"font-size: 14px;\">&nbsp;It Can be used as sports {mycket bra|v?ldigt bra|vettig} as it can {extend|stretch out|expand} at comfort</span></font></div><div><font color=\"#9d9d9d\" face=\"Roboto, Helvetica Neue, Helvetica, Arial, sans-serif\"><span style=\"font-size: 14px;\">&nbsp;Prefect {fitted|appropriate|suitable} to all {size and shapes|sizes and shapes|shapes and forms}</span></font></div><div><font color=\"#9d9d9d\" face=\"Roboto, Helvetica Neue, Helvetica, Arial, sans-serif\"><span style=\"font-size: 14px;\">{Ideal|Perfect|Top} quality fiber, contains 92% Nylon and 8 % spandex</span></font></div><div><font color=\"#9d9d9d\" face=\"Roboto, Helvetica Neue, Helvetica, Arial, sans-serif\"><span style=\"font-size: 14px;\">&nbsp;Machine washable and safe for machine {drying out|jerking}</span></font></div><div><font color=\"#9d9d9d\" face=\"Roboto, Helvetica Neue, Helvetica, Arial, sans-serif\"><span style=\"font-size: 14px;\">&nbsp;Highly durable in {conditions} of material and color</span></font></div><div><font color=\"#9d9d9d\" face=\"Roboto, Helvetica Neue, Helvetica, Arial, sans-serif\"><span style=\"font-size: 14px;\">Seamless and smooth {complete|end|surface finish}.</span></font></div>', '', 1, 1, '2017-07-29 06:24:57', '2017-08-07 11:16:47'),
(94, 38, 61, 0, 'SLIM N LIFT FOR WOMAN', '', '0130', '990', '1+', '', '<span style=\"font-family: \" trebuchet=\"\" ms\",=\"\" arial,=\"\" helvetica,=\"\" sans-serif;=\"\" font-size:=\"\" medium;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);\"=\"\">The slim n lift provides you a slimmer appearance, as if you have lost weight up to 10 kg. It levels the abdomen areas to give a slim look. It provides you a slim, shear and stylish look from below the chest line. It acts as a paranormal stick as it makes the belly slim and gives a sexy figure. It is a unique product as it keeps you slim when you are going to attend any function. It can be wear under all kinds of clothing including jeans, trousers, skirts etc.</span>', '', 1, 1, '2017-07-29 06:36:54', '2017-08-07 11:16:27'),
(95, 38, 61, 0, 'Hot Shapers', '', '0140', '2990', '1+', '', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">The own workout routines Assist tonificiar waist, hips and thighs Warms your body in your {day to day activities|activities}. {Offers|Provides|Features} effect to Planner\'s {belly|stomach|abdomen} You burn calories {You now|You} notice changes faster when you wear your {Warm|Sizzling|Popular} Shapers!</span></font>', '', 1, 1, '2017-07-29 06:44:38', '2017-08-07 11:16:15'),
(96, 38, 61, 0, 'Sweat Slim Belt', '', '0150', '2550', '1+', '', '<p style=\"box-sizing: content-box; margin: 0px; font-size: 12px; line-height: inherit; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-family: Arial, Helvetica, sans-senif; vertical-align: baseline; color: rgb(51, 51, 51);\"><span style=\"box-sizing: content-box; font-weight: 700;\"><span style=\"box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: inherit; vertical-align: baseline;\">Hot Thermo Sweat Neoprene body Shapers hot Slimming Belt</span></span></p><p style=\"box-sizing: content-box; margin: 0px; font-size: 12px; line-height: inherit; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-family: Arial, Helvetica, sans-senif; vertical-align: baseline; color: rgb(51, 51, 51);\"><span style=\"box-sizing: content-box; font-weight: 700;\"><span style=\"box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: inherit; vertical-align: baseline;\">Hot shaper Neoprene Sleeve belt tummy tuck sauna sweat slim belt</span></span></p><p style=\"box-sizing: content-box; margin: 0px; font-size: 12px; line-height: inherit; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-family: Arial, Helvetica, sans-senif; vertical-align: baseline; color: rgb(51, 51, 51);\"><span style=\"box-sizing: content-box; font-weight: 700;\"><span style=\"box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: inherit; vertical-align: baseline;\">Hot Thermo Sweat Neoprene Shapers Slimming Belt For Weight Loss Women &amp; Men</span></span></p><p style=\"box-sizing: content-box; margin: 0px; font-size: 12px; line-height: inherit; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-family: Arial, Helvetica, sans-senif; vertical-align: baseline; color: rgb(51, 51, 51);\"><span style=\"box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 18px; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\"><span style=\"box-sizing: content-box; font-weight: 700;\"><span style=\"box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: inherit; vertical-align: baseline;\"><em style=\"box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;\">TopNeoprene&nbsp;<span style=\"box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 19px; font-family: verdana, arial, helvetica, sans-serif; vertical-align: baseline; background-color: rgb(255, 255, 255);\"><span style=\"box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: 19px; vertical-align: baseline;\">China OEM factory</span></span></em></span><span style=\"box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 16px; line-height: 24px; font-family: inherit; vertical-align: baseline;\">&nbsp;Neoprene Tuck Sauna Sweat slim belt</span></span></span></p>', '', 1, 1, '2017-07-29 06:54:27', '2017-08-07 11:16:04'),
(98, 38, 42, 0, 'Kemei Km-2502', '', '0160', '1490', '1+', '', '<div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">{Impressive|Progressive|Ground breaking} Device Takes Chore Away Of Smoothing Skin {Around the|Within the|For the} Feet.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Gently And Effecively Buffs Away Dead, Calloused, hard and Dry {Pores and skin|Epidermis|Skin area} Seconds.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Safer and {Even more|Additional|Extra} Effective Than Scraters, {Which usually|Which in turn|Which will} Use Blades to {Slice|Minimize|Lower} the Skin.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">&nbsp;Easy And Fun to Use {In contrast to|As opposed to|Contrary to} Tomice Stones That Need Constant rubbing</span></font></div>', '', 1, 1, '2017-07-29 07:12:15', '2017-08-07 11:15:55'),
(99, 38, 42, 0, 'Power Perfect Pore', '', '0170', '1290', '1+', '', '<div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">{Comfort and ease|Convenience|Ease and comfort} and ease</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">designede suction cup.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">&nbsp;Remove black</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">&nbsp;{mind|minds|brain} without {harming|damaging|injuring}</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">your {pores and skin|epidermis|skin area} with head squeezing.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">&nbsp;{Total|Full|Finish} set 4</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">interchangeable {connection|add-on|accessory} and with mist function.</span></font></div>', '', 1, 1, '2017-07-29 07:21:19', '2017-08-07 11:15:47'),
(100, 38, 42, 0, '5 in 1 Epilator,Shaver', '', '0180', '2190', '1+', '', '<div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">{Small , and|Small ,} compact.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Light highlights all hair.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Easy epilation.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">{RED|LED PRE LIT|XMAS TREES} indicator light. Rechargeable; {Recharging|Asking} time: 10 hours.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">{Ideal for|Suited to|Well suited for} the face, legs, hands and bikini line.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Medically tested for the most sensitive areas.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Accessories Included: Adaptor, Brush, Pouch, Epilator, Shaver, Tweezer with {Mild|Lumination}, Eyelash Curler.</span></font></div>', '', 1, 1, '2017-07-30 06:49:19', '2017-08-07 11:15:05'),
(101, 38, 42, 0, 'Kemei Lady Shaver (200a)', '', '0190', '1250', '1+', '', '<div><font face=\"Arial, Verdana\" color=\"#ff6600\"><span style=\"font-size: 13.3333px; background-color: rgb(255, 255, 255);\"><b><u>Kemei Lady Shaver (200a)</u></b></span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">&nbsp;{STANDARD RECHARGEABLE|CHARGEABLE|NORMAL RECHARGEABLE} LADY SHAVER</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">1)Safe {waxing|saving} off all body parts.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">2)Removes unwanted hair from legs, face, underarms, {swimsuit|sting bikini|swimwear} line and other {delicate|very sensitive|hypersensitive} areas.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">3)Can be used anywhere effective for up to six weeks at time.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">4)Lady shave for underarms and legs.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">5)Suitable for dry as well as wet.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">6)Flexible {waxing|saving} go get the {adjusting|realignment|modification} to sensitive areas.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">7)Removale shaving head for easy cleaning and care.</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">{Make use of|Employ|Work with}: Bikini, Body, Face, Underarm</span></font></div><div><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Power Type: Battery</span></font></div>', '', 1, 1, '2017-07-30 09:39:51', '2017-08-07 11:15:15'),
(102, 38, 61, 0, 'Slim N Lift Slimming Shirt for Men', '', '070', '1290', '1+', '', '<span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">- A revolutionary tummy slimming vest can give you that V shape looking figure.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">- Made from a super comfortable fabric.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">- The most effective solution for quickly smoothing out unsightly bulges and getting your figure back.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">- Slim vest also helps support your back giving you that perfect posture.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">- Special weave of 12 pressure points in the stomach area to ensure better belly shaping.</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">- Works when you wear your Favourite shirts and T-shirts!</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">- No one will know your secret while you enjoy a sleek, new, slimmer you!</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">Colors : White &amp; Black</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">Materials : 80% Nylon and 20% Spandex</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\"><span style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">Size available : S, M,L,XL</span><br style=\"color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 12px; background-color: rgb(255, 255, 255);\">', '', 1, 1, '2017-07-30 12:58:15', '2017-08-07 11:15:29'),
(103, 35, 51, 0, 'Multi Functional Hand Juicer', '', '0200', '1280', '1+', '', '<h3 class=\"view_pro_name\" style=\"margin: 20px 0px 0px 10px; padding: 0px; height: 40px;\"><ul><li><span style=\"font-weight: normal;\">{Multiple|Variable|Numerous} Functional Hand Juicer:</span></li><li><span style=\"font-weight: normal;\">{Explanations|Information|Points}:</span></li><li><span style=\"font-weight: normal;\">100% brand new and high quality with competitive price.</span></li><li><span style=\"font-weight: normal;\">Ideal for {fruits|berry|berries}, vegetables, {plus more|and even more}.</span></li><li><span style=\"font-weight: normal;\">Simple to use and easy to clean.</span></li><li><span style=\"font-weight: normal;\">Food grade materials, safe and healthy.</span></li><li><span style=\"font-weight: normal;\">{The|Their|It is} unique single-auger juicer design uses an extremely {mild|soft|delicate} and efficient process to extract more high quality juice without electrical {electric power|electricity|ability}.</span></li><li><font face=\"Verdana, Arial, Helvetica, sans-serif\"><span style=\"font-size: 18px; font-weight: normal;\"><br></span></font></li><li><span style=\"font-weight: normal;\">Specifications:</span></li><li><span style=\"font-weight: normal;\">Type: Juicer</span></li><li><span style=\"font-weight: normal;\">Function: Gift for home use, restaurant</span></li><li><span style=\"font-weight: normal;\">Color: green</span></li><li><span style=\"font-weight: normal;\">Size: 30*13*22cm / 11. 8*5. 1*8. 7in(please in kind prevail).</span></li></ul></h3>', '', 1, 1, '2017-07-30 14:01:26', '2017-08-07 11:13:01'),
(104, 35, 51, 0, 'Jaipan Premium Blender', '', '0210', '3500', '6', '', '<div><ul><li><b><u><font color=\"#ff9900\">&nbsp;Jaipan Premium Blender</font></u></b></li><li>{Simple to use|User friendly|Simple to operate}, this particular mixer {mill|maker} runs on 750 {W|M} which saves you energy.</li><li>The mixer grinder once used {is not hard|is straightforward} and quick to clean and {does not|won\'t|will not} take a lot of fuss to conserve it.</li><li>Warranty: {one year|12 months} Service will be provided.</li><li>Origin (Country of manufacture): India</li><li>{Standards|Specs|Requirements}: 1) Jar and {Cutting blades|Rotor blades}: Stainless steel polished 2) Number of Jars: 3 Pcs 3) Jar with Leak proof LID 4) Material of Dome and Flat Cap: Poly carbonate 5) Speed: 3 {velocity|rate|acceleration} with incher and plus setting 6) Universal heavy dynamically balanced motor 7) RPM: Approx. 18000 on load 8) System {Top|Nice|Smart} abs designer body simple Clean and Wash. 9) Power: 750 watts; {Working|Functioning} voltage: 220-240 volts</li></ul></div>', '', 1, 1, '2017-07-30 14:09:02', '2017-08-23 04:44:47'),
(107, 35, 51, 0, 'Roti Maker', '', '0220', '2750', '1+', '', '<div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; outline: 0px; vertical-align: top; color: rgb(51, 51, 51); font-family: Arial, sans-serif; font-size: 14px;\"><font face=\"Arial, Verdana\" color=\"#ff6600\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top; font-size: 13.3333px;\"><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; outline: 0px; vertical-align: top;\">Roti Maker</b></span></font></div><div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; outline: 0px; vertical-align: top; color: rgb(51, 51, 51); font-family: Arial, sans-serif; font-size: 14px;\"><span style=\"font-weight: inherit;\">The Chapati Maker {can certainly|may easily} make tasty rotis (chapatis), khakaraas, papads etc in a few seconds. It has been designed to save your time and energy in the kitchen. The Roti Maker makes {standard|even|clothes} rotis which are {smooth|gentle|very soft} and remain hot for a long time.</span></div><div style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; outline: 0px; vertical-align: top; color: rgb(51, 51, 51); font-family: Arial, sans-serif; font-size: 14px;\"><ul style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top; list-style: none;\"><li style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top;\">&nbsp;Roti maker rolls and {at home cooks|chefs} chapattis in few {mere seconds|secs|moments}</li><li style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top;\">It saves efforts, time and cooking {essential oil|olive oil|petrol}</li><li style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top;\">&nbsp;Non-stick plates cooking reduces {calorie consumption|calories from fat|unhealthy calories}</li><li style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top;\">&nbsp;Its temperature can be {handled|manipulated} by {Temperature control system|Thermal}</li><li style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top;\">{This|That} works on 230/240 {Sixth is v|Versus|Sixth v} AC, 50 Hz</li><li style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top;\">&nbsp;{This|That} consumes the power nine hundred W</li><li style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top;\">&nbsp;Roti-maker has a stainless steel body</li><li style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top;\">&nbsp;{This|That} can be {a fantastic|an outstanding} {gift idea|surprise} Item</li><li style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top;\">&nbsp;It has a great utility in types of households</li><li style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top;\">{Brand|Name brand|Manufacturer}: Jaipan</li><li style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: inherit; outline: 0px; vertical-align: top;\">&nbsp;Made in India</li></ul></div>', '', 1, 1, '2017-07-30 17:28:11', '2017-08-07 11:13:28'),
(108, 35, 51, 0, 'Genius Nicer Dicer Plus', '', '0230', '1550', '1+', '', '<ul><li><b><font color=\"#ff6600\">Genius Nicer Dicer Plus</font></b></li><li>{Package|System|Set up} Includes:</li><li>&nbsp;1x cutting-top with integrated pin grid</li><li>&nbsp;1x Cover for holding a fresh collection container</li><li>1x transparent collector (capacity {you|one particular}, 500 ml)</li><li>&nbsp;1x {knife|cutting tool|cutter} assembly (6 mm {times|back button|a} 6 mm or doze mm x 12 mm)</li><li>&nbsp;1x blade assembly (6 mm x 36 {millimeter|logistik} or 18 mm {times|back button|a} 18 mm)</li><li>&nbsp;1x {blade|cutlery|cutting knife} used for quarters or eighths</li><li>&nbsp;1x plug-cutting {strike|impact|hand techinque} for 8th</li><li>&nbsp;1x part-cover for all blade inserts</li><li>&nbsp;1x professional peeler</li><li>&nbsp;1x cutting-base<br></li></ul>', '', 1, 1, '2017-07-30 17:56:01', '2017-08-07 11:13:40'),
(109, 36, 54, 0, '6-Way Manual Treadmill (Total 6 Item)', '', '0250', '19500', '1+', '', '<ul><li><h3 class=\"view_pro_name\" style=\"margin: 20px 0px 0px 10px; padding: 0px; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 18px; height: 40px;\"><span style=\"font-weight: normal;\"><font color=\"#ff6666\">6-Way Manual Treadmill (Total 6 Item)</font></span></h3></li><li style=\"font-weight: normal;\">Manual Treadmill six-function, total 6 item, waist twister, sit-up bar, running, push up bar, body massager, elastic band, 150 KG maximum user weight, easy to use foldable and movable, digital features - time / speed / distance / calorie / puls.</li></ul>', '', 1, 1, '2017-07-30 19:05:28', '2017-08-07 11:01:11'),
(110, 33, 34, 0, 'product_size', 'null', 'demo', '1212', '11', '1', 'e3e3', 'null', 1, 1, '2017-07-31 03:08:55', '2017-07-31 03:08:55'),
(111, 37, 55, 0, 'SMART WATCH MOBILE', '', '0260', '2150', '1+', '5', '<div><font face=\"Arial, Verdana\" color=\"#ff0000\"><span style=\"font-size: 13.3333px;\"><b>SMART WATCH MOBILE</b></span></font></div><div><ul><li>{Solitary|One|Sole} SIM Card Single Life</li><li>&nbsp;GSM quad {music group|strap|group} 850/900/1800/1900MHz</li><li>&nbsp;Bluetooth 3. 0 version</li><li>{Memory space|Storage|Recollection}: 128M+64M, support 32GB TF {cards|credit card|greeting card}</li><li>1. 3 mp camera</li><li>&nbsp;Support NFC function</li><li>&nbsp;Support Waterproof function</li><li>&nbsp;Support compass</li><li>1. fifty four? capacitive HD screen, 240X240 {quality|image resolution}</li><li>&nbsp; Support answer/ make calling by the watch directly receive or &nbsp;send {communications|text messages|emails}</li><li>{Synchronize|Connect} music. {MP3 FORMAT|AUDIO}</li><li>{Synchronize|Connect} whatsApp, skype, MSN, wechat, SMS and so {on|out|up} between watch and iPhone/ Android phone</li><li>{Measuring instrument|Measuring device|Measuring system}, Sleep monitoring</li><li>{Electric battery|Battery pack|Power supply} capacity: 3. 7V/ 450mAh</li><li>Color: Black+Siver</li></ul></div>', '', 1, 1, '2017-08-01 20:24:15', '2017-08-07 11:11:17'),
(112, 37, 55, 0, 'Apple Smart Watch', '', '0270', '3490', '1+', '5', '<div><font face=\"Arial, Verdana\" color=\"#ff0000\"><span style=\"font-size: 13.3333px;\"><b>Apple Smart Watch</b></span></font></div><div><ul><li>{IT IS JUST A|THIS CAN BE A} REPLICA ITEAM</li><li>&nbsp;CPU MTK6260A</li><li>&nbsp;Memory 128M+64M Support {maximum|greatest extent|utmost} 32GB TF card</li><li>Screen 1. 5\" inch THIN FILM TRANSISTOR HD LCDResolution ratio240*240 {-pixel|cote|nullement}</li><li>&nbsp;Touch screen OGS capacitive {display|display screen}</li><li>Bluetooth Ver. 3. zero</li><li>Camera 0. 3M</li><li>Battery 850mAh;</li><li>Stand by: More than 7days;</li><li>{Make use of|Employ|Work with} for: More than 2days</li><li>Acceleration sensor Support: {THREE DIMENSIONAL|3 DIMENSIONAL|3D IMAGES} Acceleration, Step gauge {evaluation|research|examination}, Sedentary remind, Sleep monitoring, Anti lost, Remote picture.</li><li>G-sensor: Yes</li><li>NFC: {Undoubtedly|Absolutely}</li></ul></div>', '', 1, 1, '2017-08-01 20:36:01', '2017-08-01 20:38:33'),
(113, 37, 55, 0, 'Spy Camera Pen (32GB Memory Free)', '', '0280', '2550', '1+', '', '<div><font face=\"Arial, Verdana\" color=\"#ff0000\"><span style=\"font-size: 13.3333px;\"><b>Spy Camera Pen (32GB Memory Free)</b></span></font></div><div><ul><li>{Secret agent|Traveler|Criminal} Camera Pen (Free {32 GB|32GIG|32-GB} Memory) - Use it today to store {your entire|your} security and emergency data.</li><li>? It can be took pictures of, even video can be recorded. The video {saving|tracking|taking} enables you to see very cleverly with {hearing|being attentive|tuning in}. Also you can {put it to use|make use of it} as a Pen {Travel|Get|Disk drive}.</li><li>? You will get a 5 megapixel video camera pane with 32 {GIGABYTE|GIGABITE|GIG} fixed memory. Its Online video Resolution: 1280 * 960 VGA. Battery Type: High-power lithium polymer and {box|bundle|supply} contains 1 Spy Online video Pen Camera &amp; {you|one particular} USB cable.</li></ul></div>', '', 1, 1, '2017-08-01 20:46:33', '2017-08-07 11:08:54');
INSERT INTO `products` (`id`, `category_id`, `sub_category_id`, `sub_sub_category_id`, `product_name`, `product_name_bn`, `product_code`, `product_price`, `product_quantity`, `discount`, `description`, `description_bn`, `offer_status`, `publication_status`, `created_at`, `updated_at`) VALUES
(114, 37, 55, 0, 'Mini Key Chain Hidden Camera', '', '0290', '3450', '1+', '', '<div><font face=\"Arial, Verdana\" color=\"#ff0000\"><span style=\"font-size: 13.3333px;\"><b>Mini Key Chain Hidden Camera</b></span></font></div><div><ul><li>Online video Format: 720*480 High {Quality|Image resolution} AVI with 30 frames per second</li><li>Picture Format: 1280*1024 {-pixels|px|pxs} resolution in JPEG format</li><li>Flash Drive: For easy transfer &amp; storage of electronic {documents|data files|data}</li><li>Battery: {Large|Great|Superior} Capacity Polymer Li-in {Electric battery|Battery pack|Power supply} for about two several hours of recording with {audio|appear}</li><li>Includes Car Remote {Important|Crucial|Essential} Chain Mini DVR, {UNIVERSAL SERIAL BUS|UNIVERSAL SERIES BUS|HARDWARE} Cable, E-Manual</li></ul></div>', '', 1, 1, '2017-08-01 20:52:59', '2017-08-07 11:09:50'),
(118, 35, 51, 0, 'TVC Power Bullet', '', '0330', '3450', '1+', '', '<span style=\"font-family: \" trebuchet=\"\" ms\",=\"\" arial,=\"\" helvetica,=\"\" sans-serif;=\"\" font-size:=\"\" medium;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);\"=\"\">The As Seen on TV (Tvc Power Bullet ) 21 piece set can do virtually any job in the kitchen in ten seconds or less. This handy countertop appliance can chop, mix, blend, whip, grind, and more. It\'s great for making refreshing smoothies, shakes, zesty salsas, or mouthwatering desserts.</span>', '', 1, 1, '2017-08-07 19:40:09', '2017-08-07 19:41:35'),
(119, 35, 51, 0, 'Easy Atta Making', 'null', '0340', '1250', '1+', '', '<h3 class=\"view_pro_name\" style=\"margin: 20px 0px 0px 10px; padding: 0px; height: 40px;\">Easy Atta Making</h3><div><span style=\"font-family: &quot;Trebuchet MS&quot;, Arial, Helvetica, sans-serif; background-color: rgb(255, 255, 255);\">Place the blade in the bowl over the bottom grove properly.Put atta/maida,water &amp; oil in the proportion 1:1:1 with given measuring cups,close the lid and fix the handle properly.Now rotate the handle for about 2 minutes (15 to 20 times first clockwise and then anti clockwise) you can see inside the bowl with the help of see through removable cover.You can also add water or oil through the hole without removing the lid.Now your dough / mixture is ready to prepare the rotis.</span></div>', 'null', 1, 1, '2017-08-07 19:55:38', '2017-08-07 19:55:38'),
(120, 35, 51, 0, 'Nicer Dicer Fusion', '', '0350', '1290', '1+', '', '<h3>Nicer Dicer Fusion</h3>\r\n\r\n<ul>\r\n	<li>&nbsp;Nicer Dicer Fusion - Chopper &amp; Slicer</li>\r\n	<li>The versatile food preparation station</li>\r\n	<li>Introducing the Nicer Dicer Fusion from Thane, the versatile food preparation station that slices, dices, chops, juliennes, shreds, cubes, and quarters all in one kitchen set that prepares food and family meals, faster than you ever thought possible!</li>\r\n</ul>\r\n', '', 2, 1, '2017-08-07 20:05:51', '2017-08-30 07:07:36');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `product_image_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`product_image_id`, `product_id`, `product_image`, `created_at`, `updated_at`) VALUES
(8, 4, 'downloadsadasdas.jpg', '2017-01-03 21:50:08', '2017-01-03 21:50:08'),
(9, 4, 'f2.jpg', '2017-01-03 21:50:08', '2017-01-03 21:50:08'),
(10, 20, 'product_image\\bkash.jpg', '2017-01-04 06:38:09', '2017-01-04 06:38:09'),
(11, 20, 'product_image\\cad1.jpg', '2017-01-04 06:38:09', '2017-01-04 06:38:09'),
(12, 10, 'cad1.jpg', '2017-01-04 07:02:19', '2017-01-04 07:02:19'),
(14, 46, 'product_image\\asdas.jpg', '2017-01-04 21:46:33', '2017-01-04 21:46:33'),
(15, 46, 'product_image\\asdasd.jpg', '2017-01-04 21:46:33', '2017-01-04 21:46:33'),
(16, 47, 'product_image\\asdas.jpg', '2017-01-04 21:51:10', '2017-01-04 21:51:10'),
(17, 47, 'product_image\\cad2.jpg', '2017-01-04 21:51:10', '2017-01-04 21:51:10'),
(19, 10, 'product_image\\f2.jpg', '2017-01-05 01:25:43', '2017-01-05 01:25:43'),
(20, 48, 'product_image\\b1-1.jpg', '2017-01-05 06:25:35', '2017-01-05 06:25:35'),
(21, 48, 'product_image\\b2.jpg', '2017-01-05 06:25:35', '2017-01-05 06:25:35'),
(22, 49, 'product_image\\739d383c78a4b2d66dc8414f0f2f9976.jpg', '2017-01-08 07:46:33', '2017-01-08 07:46:33'),
(23, 49, 'product_image\\1681160-transcends-original-imae6r6quszhbkgp.jpeg', '2017-01-08 07:46:33', '2017-01-08 07:46:33'),
(24, 49, 'product_image\\Archies-Peacock-Green-Ceramic-Showpiece-7005-721883-1-pdp_slider_m.jpg', '2017-01-08 07:46:33', '2017-01-08 07:46:33'),
(25, 49, 'product_image\\Paras-Green-Peacock-Showpiece-SDL356069069-1-2b687.jpg', '2017-01-08 07:46:33', '2017-01-08 07:46:33'),
(26, 50, 'product_image\\images.jpg', '2017-01-08 07:51:29', '2017-01-08 07:51:29'),
(27, 50, 'product_image\\Paras-Royal-Romantic-Couple-Showpiece-SDL894211066-1-d2670.jpg', '2017-01-08 07:51:29', '2017-01-08 07:51:29'),
(28, 51, 'product_image\\images (2).jpg', '2017-01-08 07:54:17', '2017-01-08 07:54:17'),
(29, 51, 'product_image\\NVR-Multicolour-Water-Fountain-Showpiece-SDL190075992-1-44bad.jpg', '2017-01-08 07:54:17', '2017-01-08 07:54:17'),
(30, 52, 'product_image\\images (3).jpg', '2017-01-08 07:55:33', '2017-01-08 07:55:33'),
(31, 52, 'product_image\\images (4).jpg', '2017-01-08 07:55:33', '2017-01-08 07:55:33'),
(32, 52, 'product_image\\sa020-sancheti-art-400x400-imaefa8tgazdcykj.jpeg', '2017-01-08 07:55:33', '2017-01-08 07:55:33'),
(33, 53, 'product_image\\hm2.jpg', '2017-01-10 00:14:33', '2017-01-10 00:14:33'),
(34, 53, 'hm3.jpg', '2017-01-10 00:14:33', '2017-01-10 00:14:33'),
(35, 54, 'product_image\\hm4.jpg', '2017-01-10 00:16:55', '2017-01-10 00:16:55'),
(36, 54, 'product_image\\hm5.jpg', '2017-01-10 00:16:55', '2017-01-10 00:16:55'),
(37, 55, 'product_image\\cl1.jpg', '2017-01-10 00:18:32', '2017-01-10 00:18:32'),
(38, 55, 'product_image\\cl2.jpg', '2017-01-10 00:18:32', '2017-01-10 00:18:32'),
(39, 56, 'product_image\\cl3.jpg', '2017-01-10 00:20:33', '2017-01-10 00:20:33'),
(40, 56, 'product_image\\cl4.jpg', '2017-01-10 00:20:33', '2017-01-10 00:20:33'),
(41, 57, 'product_image\\j1.jpg', '2017-01-10 00:22:27', '2017-01-10 00:22:27'),
(42, 57, 'product_image\\j2.jpg', '2017-01-10 00:22:27', '2017-01-10 00:22:27'),
(43, 58, 'product_image\\17015346_661932453986413_435113642_o.jpg', '2017-05-08 06:42:35', '2017-05-08 06:42:35'),
(44, 58, 'cb-29_1.jpg', '2017-05-08 06:42:35', '2017-05-08 06:42:35'),
(45, 59, '15697758_1192167880820956_556540586274312860_n.jpg', '2017-05-08 23:15:29', '2017-05-08 23:15:29'),
(46, 60, '299.jpg', '2017-05-08 23:17:02', '2017-05-08 23:17:02'),
(47, 61, '15781207_1192167537487657_6975721031401627982_n.jpg', '2017-05-08 23:18:40', '2017-05-08 23:18:40'),
(48, 62, '1-250x250.jpg', '2017-05-08 23:20:46', '2017-05-08 23:20:46'),
(49, 62, '15697365_1192167547487656_6482461259784418848_n.jpg', '2017-05-08 23:20:46', '2017-05-08 23:20:46'),
(50, 63, 'Hospital Furniture.jpg', '2017-05-15 00:56:41', '2017-05-15 00:56:41'),
(51, 64, 'Home Furniture.jpg', '2017-05-15 00:58:38', '2017-05-15 00:58:38'),
(52, 65, 'Interior.jpg', '2017-05-15 01:03:00', '2017-05-15 01:03:00'),
(53, 66, 'Up Coming Products..jpg', '2017-05-15 02:15:14', '2017-05-15 02:15:14'),
(54, 67, 'SAVE UP TO 50% OFF. COST EFFECTIVITY.jpg', '2017-05-15 02:16:14', '2017-05-15 02:16:14'),
(55, 68, 'INDUSTRIAL FURNITURE.jpg', '2017-05-15 02:18:31', '2017-05-15 02:18:31'),
(56, 69, 'Victorian breakfront display cabinet DP35.JPG', '2017-05-15 02:19:27', '2017-05-15 02:19:27'),
(59, 71, 'fdsfsdfsd.jpeg', '2017-06-08 03:16:54', '2017-06-08 03:16:54'),
(60, 71, 'fgfgdfgf.jpg', '2017-06-08 03:16:54', '2017-06-08 03:16:54'),
(61, 71, 'gdsgdfgdfsgdfg.jpg', '2017-06-08 03:16:54', '2017-06-08 03:16:54'),
(62, 72, 'asdasdasd.jpg', '2017-06-08 03:23:41', '2017-06-08 03:23:41'),
(63, 72, 'dfsgdfgdsfgrrr.jpg', '2017-06-08 03:23:41', '2017-06-08 03:23:41'),
(65, 73, 'gdsgdfgdfsgdfg.jpg', '2017-06-08 03:25:50', '2017-06-08 03:25:50'),
(66, 73, 'images (1).jpg', '2017-06-08 03:25:50', '2017-06-08 03:25:50'),
(67, 74, 'GENTS-JEANS-PENT.jpg', '2017-06-08 03:52:06', '2017-06-08 03:52:06'),
(68, 74, 'images (2).jpg', '2017-06-08 03:52:06', '2017-06-08 03:52:06'),
(69, 74, 'sadfasdasd.jpg', '2017-06-08 03:52:06', '2017-06-08 03:52:06'),
(70, 75, 'd fdh dfh df.jpg', '2017-06-10 21:44:06', '2017-06-10 21:44:06'),
(71, 75, 'imagessdf sdf.jpg', '2017-06-10 21:44:06', '2017-06-10 21:44:06'),
(72, 76, 'asdasdas.jpg', '2017-06-11 02:43:27', '2017-06-11 02:43:27'),
(73, 76, 'images (2).jpg', '2017-06-11 02:43:27', '2017-06-11 02:43:27'),
(74, 77, 'downloadsadasdas.jpg', '2017-06-11 02:59:06', '2017-06-11 02:59:06'),
(75, 77, 'drjhghj.jpg', '2017-06-11 02:59:06', '2017-06-11 02:59:06'),
(76, 78, '11460104200751-Highlander-Blue-Slim-Fit-Denim-Shirt-7421460104200117-1.jpg', '2017-06-11 03:17:36', '2017-06-11 03:17:36'),
(77, 78, 'd fdh dfh df.jpg', '2017-06-11 03:17:36', '2017-06-11 03:17:36'),
(78, 79, 'asdasdasd.jpg', '2017-06-12 00:07:48', '2017-06-12 00:07:48'),
(79, 79, 'dfsgdfgdsfgrrr.jpg', '2017-06-12 00:07:48', '2017-06-12 00:07:48'),
(81, 80, 'cream-punjabi-wedding-sherwani-in-jacquard-h15283-411.jpg', '2017-06-13 00:04:52', '2017-06-13 00:04:52'),
(82, 80, 'download (2).jpg', '2017-06-13 00:04:52', '2017-06-13 00:04:52'),
(96, 88, '0b02c8b3332d77736661a4d63ca5c37a.jpg', '2017-07-25 02:51:11', '2017-07-25 02:51:11'),
(97, 88, 'images.jpg', '2017-07-25 02:51:11', '2017-07-25 02:51:11'),
(98, 88, 'images4OWO62O0.jpg', '2017-07-25 02:51:11', '2017-07-25 02:51:11'),
(103, 91, 'Derma Seta 0100b.jpg', '2017-07-27 09:16:24', '2017-07-27 09:16:24'),
(106, 93, 'air-bra.jpg', '2017-07-29 06:24:57', '2017-07-29 06:24:57'),
(107, 94, '0130.jpg', '2017-07-29 06:36:54', '2017-07-29 06:36:54'),
(108, 94, '0130a.jfif', '2017-07-29 06:36:54', '2017-07-29 06:36:54'),
(109, 94, '0130b.jpg', '2017-07-29 06:36:54', '2017-07-29 06:36:54'),
(110, 94, '0130c.jpg', '2017-07-29 06:36:54', '2017-07-29 06:36:54'),
(111, 95, '0140.png', '2017-07-29 06:44:38', '2017-07-29 06:44:38'),
(112, 95, '0140A.jpg', '2017-07-29 06:44:38', '2017-07-29 06:44:38'),
(113, 95, '0140B.jpg', '2017-07-29 06:44:38', '2017-07-29 06:44:38'),
(114, 95, '0140C.jpg', '2017-07-29 06:44:38', '2017-07-29 06:44:38'),
(115, 96, '0150.jpg', '2017-07-29 06:54:27', '2017-07-29 06:54:27'),
(116, 96, '0150A.jpg', '2017-07-29 06:54:27', '2017-07-29 06:54:27'),
(117, 96, '0150B.jpg', '2017-07-29 06:54:27', '2017-07-29 06:54:27'),
(124, 98, '0160A.jpg', '2017-07-29 07:12:15', '2017-07-29 07:12:15'),
(125, 98, '0160B.jpg', '2017-07-29 07:12:15', '2017-07-29 07:12:15'),
(126, 98, '0160C.jpg', '2017-07-29 07:12:15', '2017-07-29 07:12:15'),
(127, 98, '0160D.jpg', '2017-07-29 07:12:15', '2017-07-29 07:12:15'),
(128, 98, '0160E.jpg', '2017-07-29 07:12:15', '2017-07-29 07:12:15'),
(129, 99, '0170.jpg', '2017-07-29 07:21:19', '2017-07-29 07:21:19'),
(130, 99, '0170A.jfif', '2017-07-29 07:21:19', '2017-07-29 07:21:19'),
(131, 99, '0170B.jpg', '2017-07-29 07:21:19', '2017-07-29 07:21:19'),
(132, 100, '0180.jpg', '2017-07-30 06:49:19', '2017-07-30 06:49:19'),
(133, 100, '0180a.jpg', '2017-07-30 06:49:19', '2017-07-30 06:49:19'),
(134, 100, '0180b.jpg', '2017-07-30 06:49:19', '2017-07-30 06:49:19'),
(135, 101, '0190.jpg', '2017-07-30 09:39:51', '2017-07-30 09:39:51'),
(136, 101, '0190c.jpg', '2017-07-30 09:39:51', '2017-07-30 09:39:51'),
(137, 102, 'SliM N Lift Slimming Shirt for Men 070a.jpg', '2017-07-30 12:58:15', '2017-07-30 12:58:15'),
(138, 102, 'SliM. N Lift Slimming Shirt for Men 070.jpg', '2017-07-30 12:58:15', '2017-07-30 12:58:15'),
(139, 103, '0200.jpg', '2017-07-30 14:01:26', '2017-07-30 14:01:26'),
(140, 103, '0200a.jpg', '2017-07-30 14:01:26', '2017-07-30 14:01:26'),
(141, 103, '0200b.jpg', '2017-07-30 14:01:26', '2017-07-30 14:01:26'),
(142, 104, '0210.jpg', '2017-07-30 14:09:02', '2017-07-30 14:09:02'),
(143, 104, '0210a.jpg', '2017-07-30 14:09:02', '2017-07-30 14:09:02'),
(144, 104, '0210b.jpeg', '2017-07-30 14:09:02', '2017-07-30 14:09:02'),
(153, 107, '0220.JPG', '2017-07-30 17:28:11', '2017-07-30 17:28:11'),
(154, 107, '0220a.jpg', '2017-07-30 17:28:11', '2017-07-30 17:28:11'),
(155, 107, '0220b.jpg', '2017-07-30 17:28:11', '2017-07-30 17:28:11'),
(156, 107, '0220c.jpg', '2017-07-30 17:28:11', '2017-07-30 17:28:11'),
(157, 108, '0230.jpg', '2017-07-30 17:56:01', '2017-07-30 17:56:01'),
(158, 108, '0230a.jpg', '2017-07-30 17:56:01', '2017-07-30 17:56:01'),
(159, 108, '0230b.jpg', '2017-07-30 17:56:01', '2017-07-30 17:56:01'),
(160, 108, '0230c.jpg', '2017-07-30 17:56:01', '2017-07-30 17:56:01'),
(161, 109, '0250.jpg', '2017-07-30 19:05:28', '2017-07-30 19:05:28'),
(162, 109, '0250A.jpg', '2017-07-30 19:05:28', '2017-07-30 19:05:28'),
(163, 110, '398 x 332.png', '2017-07-31 03:08:55', '2017-07-31 03:08:55'),
(165, 111, '0260.jpg', '2017-08-01 20:24:15', '2017-08-01 20:24:15'),
(166, 111, '0260a.jpg', '2017-08-01 20:24:15', '2017-08-01 20:24:15'),
(167, 112, '0270.jpg', '2017-08-01 20:36:01', '2017-08-01 20:36:01'),
(168, 112, '0270b.PNG', '2017-08-01 20:36:01', '2017-08-01 20:36:01'),
(169, 113, '0280.jpg', '2017-08-01 20:46:33', '2017-08-01 20:46:33'),
(170, 113, '0280a.jpg', '2017-08-01 20:46:33', '2017-08-01 20:46:33'),
(171, 114, '0290.jpg', '2017-08-01 20:52:59', '2017-08-01 20:52:59'),
(187, 118, '0330.png', '2017-08-07 19:40:09', '2017-08-07 19:40:09'),
(188, 118, '0330b.jpg', '2017-08-07 19:40:09', '2017-08-07 19:40:09'),
(189, 118, '0330c.JPG', '2017-08-07 19:40:09', '2017-08-07 19:40:09'),
(190, 118, '0330d.jpg', '2017-08-07 19:40:09', '2017-08-07 19:40:09'),
(191, 119, '0340.jpg', '2017-08-07 19:55:38', '2017-08-07 19:55:38'),
(192, 119, '0340a.jpg', '2017-08-07 19:55:38', '2017-08-07 19:55:38'),
(193, 119, '0340b.jpg', '2017-08-07 19:55:38', '2017-08-07 19:55:38'),
(194, 120, '0350.jpg', '2017-08-07 20:05:51', '2017-08-07 20:05:51'),
(195, 120, '0350a.jpg', '2017-08-07 20:05:51', '2017-08-07 20:05:51');

-- --------------------------------------------------------

--
-- Table structure for table `product_sizes`
--

CREATE TABLE `product_sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_sizes`
--

INSERT INTO `product_sizes` (`id`, `product_id`, `size`, `created_at`, `updated_at`) VALUES
(1, 79, 'XL', '2017-06-18 00:34:28', '2017-06-18 00:34:28'),
(3, 72, 'L', '2017-06-18 00:48:54', '2017-06-18 00:48:54'),
(4, 72, 'M', '2017-06-18 00:48:59', '2017-06-18 00:48:59'),
(5, 72, 'S', '2017-06-18 00:49:05', '2017-06-18 00:49:05'),
(6, 70, 'L', '2017-07-27 02:39:30', '2017-07-27 02:39:30'),
(7, 70, 'XL', '2017-07-27 02:39:44', '2017-07-27 02:39:44'),
(8, 71, 'S', '2017-07-30 12:24:59', '2017-07-30 12:24:59'),
(9, 71, 'L', '2017-07-30 12:25:07', '2017-07-30 12:25:07'),
(10, 70, 'XXL', '2017-07-31 03:12:25', '2017-07-31 03:12:25'),
(11, 73, 'L', '2017-08-23 04:59:45', '2017-08-23 04:59:45'),
(12, 73, 'XL', '2017-08-23 04:59:53', '2017-08-23 04:59:53'),
(13, 71, 'M', '2017-08-27 07:38:02', '2017-08-27 07:38:02'),
(14, 78, 'S', '2017-08-31 02:58:10', '2017-08-31 02:58:10'),
(15, 78, 'M', '2017-08-31 02:58:15', '2017-08-31 02:58:15'),
(16, 78, 'L', '2017-08-31 02:58:22', '2017-08-31 02:58:22'),
(17, 78, 'Xl', '2017-08-31 02:58:28', '2017-08-31 02:58:28'),
(18, 78, 'XXl', '2017-08-31 02:58:40', '2017-08-31 02:58:40');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_addresses`
--

CREATE TABLE `shipping_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipping_addresses`
--

INSERT INTO `shipping_addresses` (`id`, `order_number`, `name`, `email`, `phone`, `address`, `zipcode`, `location`, `created_at`, `updated_at`) VALUES
(26, '19279', 'Akash Akram', 'admin@gmail.com', '012354788', 'dhaka', '1234', '15', '2017-08-31 05:17:00', '2017-08-31 05:17:00'),
(27, '18139', 'demo User', 'demo@ezbazzar.com', '32324242', 'Banani', '1310', '15', '2017-09-04 21:36:17', '2017-09-04 21:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `slider_images`
--

CREATE TABLE `slider_images` (
  `slider_image_id` int(10) UNSIGNED NOT NULL,
  `slider_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `subtitle` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slider_images`
--

INSERT INTO `slider_images` (`slider_image_id`, `slider_image`, `publication_status`, `created_at`, `updated_at`, `title`, `subtitle`) VALUES
(31, 'gPRoZhBpHhbzFk7ORKWm.png', 1, '2017-07-27 09:02:34', '2017-08-30 06:52:27', 'Title', 'Subtitle'),
(33, 'tttt.jfif', 1, '2017-07-31 12:59:18', '2017-08-01 19:39:26', NULL, NULL),
(34, '17342790_280182719082590_3000772528565420074_n.jpg', 1, '2017-08-01 19:40:38', '2017-08-01 19:40:38', NULL, NULL),
(35, '8rFuOX83GrIcicEl8USR.jpg', 1, '2017-08-22 02:39:10', '2017-08-22 03:24:46', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscribes`
--

CREATE TABLE `subscribes` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subscribes`
--

INSERT INTO `subscribes` (`id`, `email`, `created_at`, `updated_at`) VALUES
(2, 'mahmud.agvbd@gmail.com', '2017-05-15 05:24:41', '2017-05-15 05:24:41'),
(3, 'cyndiocfa@comcast.net', '2017-07-14 13:03:43', '2017-07-14 13:03:43'),
(4, 'rjostant@gmail.com', '2017-07-14 15:40:47', '2017-07-14 15:40:47'),
(5, 'msteed@txkusa.org', '2017-07-14 18:59:17', '2017-07-14 18:59:17'),
(6, 'pthompson@thompsone.com', '2017-07-14 23:18:04', '2017-07-14 23:18:04'),
(7, 'illmind828@gmail.com', '2017-07-14 23:34:41', '2017-07-14 23:34:41'),
(8, 'iamking.tg@gmail.com', '2017-07-15 00:57:12', '2017-07-15 00:57:12'),
(9, 'bkwilliamspmp@gmail.com', '2017-07-16 08:11:36', '2017-07-16 08:11:36'),
(12, 'vishal.batra75@gmail.com', '2017-07-16 18:27:11', '2017-07-16 18:27:11'),
(13, 'shazzadurrahaman@gmail.com', '2017-07-17 05:25:50', '2017-07-17 05:25:50'),
(14, '6144965638@txt.att.net', '2017-07-18 08:45:57', '2017-07-18 08:45:57'),
(15, 'k.kastendieck@t-online.de', '2017-07-18 11:14:58', '2017-07-18 11:14:58'),
(16, 'mercier.br@wanadoo.fr', '2017-07-18 13:39:50', '2017-07-18 13:39:50'),
(17, 'jesslom2000@yahoo.com', '2017-07-18 15:17:09', '2017-07-18 15:17:09'),
(18, 'david.korn.cpa@gmail.com', '2017-07-18 15:42:39', '2017-07-18 15:42:39'),
(19, 'chrishale@gmail.com', '2017-07-18 22:13:56', '2017-07-18 22:13:56'),
(20, 'koolgirl1997@yahoo.com', '2017-07-19 01:39:47', '2017-07-19 01:39:47'),
(21, 'a.page@labtech.com', '2017-07-19 02:17:54', '2017-07-19 02:17:54'),
(22, 'johnsonsk@siouxvalley.net', '2017-07-19 02:28:48', '2017-07-19 02:28:48'),
(23, 'robertpp2@comcast.net', '2017-07-20 11:02:10', '2017-07-20 11:02:10'),
(24, 'jodavi2001@yahoo.com', '2017-07-20 19:37:42', '2017-07-20 19:37:42'),
(25, 'pdreger@gmail.com', '2017-07-21 01:22:33', '2017-07-21 01:22:33'),
(26, 'crazy4bikes@gmail.com', '2017-07-21 10:39:54', '2017-07-21 10:39:54'),
(27, 'connanbar@gmail.com', '2017-07-21 17:00:23', '2017-07-21 17:00:23'),
(28, 'avimscher@gmail.com', '2017-07-21 21:50:22', '2017-07-21 21:50:22'),
(29, 'admin@gmail.com', '2017-09-04 23:09:50', '2017-09-04 23:09:50'),
(30, 'mahmud@gmail.com', '2017-09-05 01:38:14', '2017-09-05 01:38:14');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `sub_category_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_category_name_bn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`sub_category_id`, `category_id`, `sub_category_name`, `sub_category_name_bn`, `publication_status`, `created_at`, `updated_at`) VALUES
(34, 33, 'MEN\'S CLOTHING', 'মেন্স ক্লোথিং', 1, '2017-06-07 22:39:25', '2017-08-24 03:33:06'),
(35, 33, 'TRADITIONAL CLOTHING', 'ট্রাডিশনাল ক্লোথিং', 1, '2017-06-07 22:43:19', '2017-08-24 03:33:08'),
(36, 33, 'MEN\'S ACCESSORIES', 'মেন্স একসেসোরিজ', 1, '2017-06-07 22:43:57', '2017-06-07 22:43:57'),
(37, 33, 'MEN\'S SHOES', 'মেন্স সুস ', 1, '2017-06-07 22:47:31', '2017-06-07 22:47:31'),
(38, 33, 'INNERWEAR & NIGHTWEAR', 'ইননেরওয়ার  & নিঘ্ত্বের ', 1, '2017-06-07 22:48:07', '2017-06-07 22:48:07'),
(39, 33, 'WINTER CLOTHING', 'উইন্টার ক্লোথিং', 1, '2017-06-07 22:50:17', '2017-06-07 22:50:17'),
(40, 33, 'MEN\'S WATCHES', 'মেন্স ওয়াচেস ', 1, '2017-06-07 22:51:16', '2017-06-07 22:51:16'),
(42, 38, 'HAIR REMOVE & FACIAL', '', 1, '2017-07-14 15:35:43', '2017-08-24 00:55:09'),
(44, 34, 'CLOTHING', 'ক্লথিং ', 1, '2017-07-25 02:46:10', '2017-07-27 10:52:50'),
(46, 34, 'HEALTH & BEAUTY', 'হেলথ এন্ড বিউটি ', 1, '2017-07-25 02:49:04', '2017-07-27 10:55:19'),
(48, 34, 'SPORTS & FITNESS', 'SPORTS & FITNESS', 1, '2017-07-26 11:02:47', '2017-07-27 10:57:02'),
(49, 34, 'ACCESSORIES', 'ACCESSORIES', 1, '2017-07-27 10:59:01', '2017-07-27 10:59:01'),
(50, 34, 'WATCHES', 'WATCHES', 1, '2017-07-27 10:59:40', '2017-07-27 10:59:40'),
(51, 35, 'KITCHEN APPLIANCES', 'KITCHEN APPLIANCES', 1, '2017-07-27 11:01:02', '2017-07-27 11:01:02'),
(52, 35, 'HOME APPLIANCES', 'HOME APPLIANCES', 1, '2017-07-27 11:01:28', '2017-07-27 11:01:28'),
(53, 35, 'HOME IMPROVEMENTS', 'HOME IMPROVEMENTS', 1, '2017-07-27 11:01:53', '2017-07-27 11:01:53'),
(54, 36, 'STORE', 'STORE', 1, '2017-07-27 11:05:17', '2017-07-27 11:05:17'),
(55, 37, 'DIGITAL SPY CAMERA', 'DIGITAL SPY CAMERA', 1, '2017-07-27 11:07:04', '2017-07-27 11:07:04'),
(56, 37, 'REHARGEABLE SHAVER', 'REHARGEABLE SHAVER', 1, '2017-07-27 11:09:36', '2017-07-27 11:09:36'),
(57, 37, 'HAIR STRAIGHTENERS', 'HAIR STRAIGHTENERS', 1, '2017-07-27 11:12:19', '2017-07-27 11:12:19'),
(58, 42, 'MAN\'S SHOES', 'MAN\'S', 1, '2017-07-27 11:26:08', '2017-07-27 11:57:14'),
(59, 42, 'WOMAN\'S', 'WOMAN\'S', 1, '2017-07-27 11:26:23', '2017-07-27 11:26:23'),
(60, 42, 'KID\'S', 'KID\'S', 1, '2017-07-27 11:26:39', '2017-07-27 11:26:39'),
(61, 38, 'SLIM & FIT ITEM', 'SLIM & FIT ITEM', 1, '2017-07-27 11:28:23', '2017-07-29 06:15:47'),
(62, 38, 'WEIGHT LOSS PRODUCTS', 'WEIGHT LOSS PRODUCTS', 1, '2017-07-27 11:29:00', '2017-07-27 11:29:00'),
(63, 38, 'OTHER APPLIANCES', 'HEIGHT GROWTH PRODUCTS', 1, '2017-07-27 11:30:01', '2017-07-27 11:34:40'),
(64, 38, 'HAIR STRAIGHTENER & HAIR DRYER ', 'HAIR STRAIGHTENER & HAIR DRYER ', 1, '2017-07-27 11:32:42', '2017-07-27 11:32:42'),
(65, 36, 'EXERCISE BIKE', 'Exercise Bike', 1, '2017-07-30 18:31:18', '2017-07-30 18:37:36'),
(66, 36, 'FITNESS EQUIPMENT', 'Fitness Equipment', 1, '2017-07-30 18:31:44', '2017-07-30 18:38:51'),
(67, 36, 'MANUAL RUNNING MACHINE', 'Manual Running Machine', 1, '2017-07-30 18:32:10', '2017-07-30 18:40:13'),
(68, 36, 'Therapy & Massager', 'Therapy & Massager', 1, '2017-07-30 18:32:39', '2017-07-30 18:32:39'),
(69, 36, 'Sliming Belt & Surgical', 'Sliming Belt & Surgical', 1, '2017-07-30 18:34:45', '2017-07-30 18:34:45'),
(70, 36, 'Body Massager', 'Body Massager', 1, '2017-07-30 18:35:04', '2017-07-30 18:35:04');

-- --------------------------------------------------------

--
-- Table structure for table `sub_sub_categories`
--

CREATE TABLE `sub_sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `sub_sub_category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_sub_category_name_bn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_sub_categories`
--

INSERT INTO `sub_sub_categories` (`id`, `sub_category_id`, `sub_sub_category_name`, `sub_sub_category_name_bn`, `publication_status`, `created_at`, `updated_at`) VALUES
(9, 34, 'T-Shirts', 'টি -শার্ট', 1, '2017-06-07 23:33:38', '2017-06-07 23:33:38'),
(10, 34, 'Polo’s', 'পোলো’স', 1, '2017-06-07 23:38:40', '2017-06-07 23:38:40'),
(11, 34, 'Shirt', 'শার্ট ', 1, '2017-06-07 23:49:07', '2017-06-07 23:49:07'),
(12, 34, 'Coats & Jackets', 'কোটস ও জ্যাকেট', 1, '2017-06-07 23:52:11', '2017-06-07 23:52:11'),
(13, 34, 'Pants', 'প্যান্টস', 1, '2017-06-07 23:59:25', '2017-06-07 23:59:25'),
(14, 34, 'Jeans', 'জিন্স', 1, '2017-06-08 00:00:39', '2017-06-08 00:00:39'),
(15, 34, 'Shorts & Barmudas', 'শর্টস & বারমুডা', 1, '2017-06-08 00:02:04', '2017-06-08 00:04:37'),
(16, 35, 'Panjabis & Sherwanis', 'পাঞ্জাবি ও শেরওয়ানি', 1, '2017-06-13 00:01:40', '2017-06-13 00:01:40'),
(17, 42, 'Cleaning appliance ', 'ক্লিনিং এপ্লায়েন্স', 1, '2017-07-14 15:40:26', '2017-07-14 15:40:26'),
(18, 42, 'Cleaning appliance ', 'ক্লিনিং এপ্লায়েন্স', 1, '2017-07-20 06:09:53', '2017-07-20 06:09:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mahmud Hira', 'info@kenakatazone.com', '$2y$10$3nzi3/N/s/1KcWR6eZAvb.tuWpu6mFBjTH90YY3DVYxdGU8YZsl3W', '2x0OrqYe9HcAwCN1nxgjsfRZ5OcRZtd8JgEhYEOsgyZrw7bSSZ8mrCOD8dgK', NULL, '2017-07-13 04:35:47'),
(2, 'Admin', 'admin@gmail.com', '$2y$10$4aD83GYfhQzUxsT2Uk50g.ra.DqIbbBMrhr9T9VOx.5W0FbxztnDi', 'yMvTOVf7T2I2cnstnKdhRr2NHV4CUeCs1xmtcSCelFzZgPuxVS1gJS5cuSDv', NULL, '2017-09-05 01:30:50'),
(3, 'Lakes Point Admin', 'admin@lakespoint.net', '$2y$10$95uQw0BuGutOB92zrZARTOshB0n3HfUbYBor8JTBiFvk.eR95hCv2', 'Ftpq88Kw1hHj5LcH84OYTvKJUZRtRu5Pw6nL7kEkjgXQLdsVYY1s4xxZPUrm', NULL, '2017-08-07 20:10:35'),
(4, 'Ezbazzar Admin', 'admin@ezbazzarbd.com', '$2y$10$Vquq6lr4YAwGCrbQWH5UkeudUZUv5vNfh1G3Dgh4cn1leAY6sjaXG', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `counter` int(11) DEFAULT '0',
  `daily_count` int(11) DEFAULT '0',
  `dhaka` int(11) DEFAULT '0',
  `chittagong` int(11) DEFAULT '0',
  `barisal` int(11) DEFAULT '0',
  `khulna` int(11) DEFAULT '0',
  `mymensingh` int(11) DEFAULT '0',
  `rajshahi` int(11) DEFAULT '0',
  `rangpur` int(11) DEFAULT '0',
  `sylhet` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `counter`, `daily_count`, `dhaka`, `chittagong`, `barisal`, `khulna`, `mymensingh`, `rajshahi`, `rangpur`, `sylhet`, `created_at`, `updated_at`) VALUES
(1, 12, 5, 4, 2, 1, 1, 0, 0, 0, 0, NULL, '17-08-13');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wishlists`
--

INSERT INTO `wishlists` (`id`, `customer_id`, `product_id`, `created_at`, `updated_at`) VALUES
(31, 10, 71, '2017-08-23 07:31:12', '2017-08-23 07:31:12'),
(37, 16, 71, '2017-09-04 23:00:40', '2017-09-04 23:00:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_to_carts`
--
ALTER TABLE `add_to_carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `pazzles`
--
ALTER TABLE `pazzles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`product_image_id`);

--
-- Indexes for table `product_sizes`
--
ALTER TABLE `product_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_images`
--
ALTER TABLE `slider_images`
  ADD PRIMARY KEY (`slider_image_id`);

--
-- Indexes for table `subscribes`
--
ALTER TABLE `subscribes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`sub_category_id`);

--
-- Indexes for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add_to_carts`
--
ALTER TABLE `add_to_carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `pazzles`
--
ALTER TABLE `pazzles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `product_image_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;
--
-- AUTO_INCREMENT for table `product_sizes`
--
ALTER TABLE `product_sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `slider_images`
--
ALTER TABLE `slider_images`
  MODIFY `slider_image_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `subscribes`
--
ALTER TABLE `subscribes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `sub_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
