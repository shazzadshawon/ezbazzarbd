<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddToCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_to_carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('product_name', 200);
            $table->string('product_name_bn', 200);
            $table->string('product_code', 200);
            $table->string('product_price', 200);
            $table->string('product_quantity', 45);
            $table->string('size', 200);
            $table->string('session_id', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_to_carts');
    }
}
