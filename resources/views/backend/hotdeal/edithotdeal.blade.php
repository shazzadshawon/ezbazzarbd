@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Update Hot deal Information</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                        <form class="form-horizontal" method="POST" action="{{ url('updatehotdeal') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}   
                            <div class="col-md-8">
                                                               
                                   
                                 

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Discount</label>
                                        <div class="col-md-10">
                                             <input id="discount" type="text" class="form-control" name="discount" value="{{ $hotdeal->discount }}"  autofocus placeholder="in percent">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Description</label>
                                        <div class="col-md-10">
                                             <textarea name="editor1">
                                                 @php
                                                     print_r($hotdeal->description);
                                                 @endphp
                                             </textarea>
                                        </div>
                                    </div>
                                    

                                    
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Select Image</label>
                                        <div class="col-md-10">
                                             <input type="file" name="hotdealimage" 
    onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" value="{{ asset('') }}">
                                <span class="help-block"></span>
                                        </div>
                                    </div>
                                    

                                      <div class="form-group">
                                        <label class="col-md-2 control-label">Date</label>
                                        <div class="col-md-10">
                                            <input type="text" id="datepicker" class="form-control" name="end_date" value="{{ $hotdeal->end_date }}">
                                        </div>
                                    </div>


                                  


                                   
                                 

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit" value="Upload" />
                                        </div>
                                    </div>

                                    
                                
                            </div>
                           <div class="col-md-4">
              
                                <img id="blah" alt="your image" class="img img-thumbnail" style="width: 300px; height: 300px;" src="{{asset('public/uploads/hotdeal/'.$hotdeal->hotdealimage)}}" />
                            </div>
                        </form>
             </div>
                 </div>
             </div>
        </div>
    </div>



@endsection