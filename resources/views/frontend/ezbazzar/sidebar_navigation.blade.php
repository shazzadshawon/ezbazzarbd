<!-- ================================== TOP NAVIGATION ================================== -->
<div class="side-menu animate-dropdown">
    <div class="head"><i class="fa fa-list"></i> all departments</div>
    <nav class="yamm megamenu-horizontal" role="navigation">
        <ul class="nav">
            @php
                $main_categories = DB::table('categories')->get();
                $i=0;
            @endphp
            @foreach($main_categories as $cat)
             <li class="dropdown menu-item">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$cat->category_name}}</a>
                <ul class="dropdown-menu mega-menu">
                    <li class="yamm-content">
                        <!-- ================================== MEGAMENU VERTICAL ================================== -->
                    @php
                    $sub_cat = DB::table('sub_categories')
                    ->where('category_id',$cat->category_id)->get();
                    @endphp
                    @include('frontend.ezbazzar.sidebar_megamenu')
                    <!-- ================================== MEGAMENU VERTICAL ================================== -->
                    </li>
                </ul>
            </li><!-- /.menu-item -->
           
            @php
            if ($i>8) {
                break;
            }
                $i++;
            @endphp
                @endforeach

            <!--            THESE ARE INSIDE SLIDEDOWN-->
            @if( count($main_categories) > 8 )
            @for ($i = 10 ; $i < count($main_categories) ; $i++ )
                 <li class="dropdown menu-item cat-link-orther">

                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$main_categories[$i]->category_name}}</a>
                <ul class="dropdown-menu mega-menu">
                    <li class="yamm-content">
                        <!-- ================================== MEGAMENU VERTICAL ================================== -->
                    @php
                    $sub_cat = DB::table('sub_categories')
                    ->where('category_id',$cat->category_id)->get();
                    @endphp
                    @include('frontend.ezbazzar.sidebar_megamenu')
                    <!-- ================================== MEGAMENU VERTICAL ================================== -->
                    </li>
                </ul>
            </li><!-- /.menu-item -->
            @endfor
           
 <div class="all-category open-cate"><span>All Categories</span></div>
            @endif

           
        </ul><!-- /.nav -->
    </nav><!-- /.megamenu-horizontal -->
</div><!-- /.side-menu -->
<!-- ================================== TOP NAVIGATION : END ================================== -->

<script type="text/javascript">
    /** ALL CAT **/
    $(document).on('click','.open-cate',function(){
        $(this).closest('.side-menu').find('li.cat-link-orther').each(function(){
            $(this).slideDown(400, function(){
                $(this).attr('style', 'overflow: show; display: block;');
            });
        });
        $(this).addClass('colse-cate').removeClass('open-cate').html('Close');
    })
    /* Close category */
    $(document).on('click','.colse-cate',function(){
        $(this).closest('.side-menu').find('li.cat-link-orther').each(function(){
            $(this).slideUp();
        });
        $(this).addClass('open-cate').removeClass('colse-cate').html('All Categories');
        return false;
    })
</script>