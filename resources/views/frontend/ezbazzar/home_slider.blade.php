<!-- ========================================== SECTION – HERO ========================================= -->

<div id="hero">
    <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
@foreach($sliders as $slider)
            <div class="item" style="background-image: url({{asset('slider_image/'.$slider->slider_image)}});">
                <div class="container-fluid">
                    <div class="caption vertical-center text-left">
                        <div class="big-text fadeInDown-1">
                            <span class="big">{{$slider->title}}</span>
                        </div>

                        <div class="excerpt fadeInDown-2">
                            {{$slider->subtitle}}<br>
                        </div>

                        <div class="button-holder fadeInDown-3">
                            <a href="#" class="big le-button ">shop now</a>
                        </div>
                    </div><!-- /.caption -->
                </div><!-- /.container-fluid -->
            </div><!-- /.item -->

 @endforeach





    </div><!-- /.owl-carousel -->
</div>

<!-- ========================================= SECTION – HERO : END ========================================= -->