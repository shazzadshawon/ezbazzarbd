@extends('layouts.frontend')

@section('content')

        <!-- ========================================= MAIN ========================================= -->
        <main id="terms" class="inner-bottom-md">
            <div class="container">
                <section class="section section-page-title wow fadeInDown">
                    <div class="page-header">
                        <h2 class="page-title">Terms and Conditions</h2>
                        <p class="page-subtitle">By using ezbazarbd.com (also referred to as ezbazar or “website”) you agree to these conditions. Please read them carefully.</p>
                    </div>
                </section><!-- /.section-page-title -->
                <section class="section intellectual-property inner-bottom-xs wow fadeInDown">
                    <h2>Communication</h2>
                    <p>When you use ezbazarbd.com, or contact us via phone or email, you consent to receive communication from us. You authorize us to communicate with you by posting disclosures on the site, by email or by phone.  Additionally any disclosures posted on the site, or sent to you by email fulfill the legal obligation of communication made in writing.</p>
                </section>
                <section class="section intellectual-property inner-bottom-xs wow fadeInDown">
                    <h2>Eligibility</h2>
                    <p>
                        If you are a minor i.e. under the age of 18 years, you shall not register as a user of the ezbazarbd.com and shall not transact on or use the website. As a minor if you wish to use or transact on website, such use or transaction may be made by your legal guardian or parents on the website. ezbazarreserves the right to terminate your membership and/or refuse to provide you with access to the website if it is brought to ezbazar’s notice or if it is discovered that you are under the age of 18 years.
                    </p>
                </section>
                <section class="section intellectual-property inner-bottom-xs wow fadeInDown">
                    <h2>Your Account and Responsibilities</h2>
                    <p>If you use the website, you will be responsible for maintaining the confidentiality of your username and password and you will be responsible for all activities that occur under your username. You agree that if you provide any information that is untrue, inaccurate, not current or incomplete or we have reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, or not in accordance with the this Terms of Use, we have the right to suspend or terminate your membership on the website.</p>
                </section>
                <section class="section intellectual-property wow fadeInDown">
                    <h2>Charges</h2>
                    <p>Membership on the website is free for users. Ezbazar does not charge any fee for browsing and buying on the website. Ezbazar reserves the right to change its fee policy from time to time. In particular, Ezbazar may at its sole discretion introduce new services and modify some or all of the existing services offered on the website. In such an event, ezbazar reserves the right to introduce fees for the new services offered or amend/introduce fees for existing services, as the case may be. Changes to the fee policy will be posted on the website and such changes will automatically become effective immediately after they are posted on the website.</p>
                </section>
                <section class="section intellectual-property wow fadeInDown">
                    <h2>Copyright</h2>
                    <p>The material (including the content, and any other content, software or services) contained on ezbazarbd.com are the property and under the license of unity fashion and lifestyle (sister concern of unity group), its subsidiaries, affiliates and/or third party licensors. No material on this site may be copied, reproduced, republished, installed, posted, transmitted, stored or distributed without written permission from ezbazarbd.com.</p>
                    <p>You may not use any “deep-link”, “page-scrape”, “robot”, “spider” or other automatic device, program, algorithm or methodology, or any similar or equivalent manual process, to access, acquire, copy or monitor any portion of the website or any content, or in any way reproduce or circumvent the navigational structure or presentation of the website or any content, to obtain or attempt to obtain any materials, documents or information through any means not purposely made available through the website. We reserve our right to bar any such activity.</p>
                </section>
                <section class="section intellectual-property wow fadeInDown">
                    <h2>Cookies </h2>
                    <p>This site uses cookies, which means that you must have cookies enabled on your computer in order for all functionality on this site to work properly. A cookie is a small data file that is written to your hard drive when you visit certain Web sites. Cookie files contain certain information, such as a random number user ID that the site assigns to a visitor to track the pages visited. A cookie cannot read data off your hard disk or read cookie files created by other sites. Cookies, by themselves, cannot be used to find out the identity of any user.</p>
                </section>
            </div>
        </main><!-- /.inner-bottom-md -->
  @endsection