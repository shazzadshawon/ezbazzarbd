    <section id="our-team">
                            <h2 class="sr-only">Our team</h2>
                            <ul class="team-members list-unstyled">

                                <li class="team-member">
                                    <img src="{{ asset('public/ezbazzar/images/team/1.jpg') }}" alt="" class="profile-pic img-responsive">
                                    <div class="profile">
                                        <h3>Azmari Huq Jalil <small class="designation">Founder &amp; CEO</small></h3>
                                        <ul class="social list-unstyled">
                                            <li>
                                                <a href="http://facebook.com/">
                                                    <span class="fa-stack fa-lg">
                                                      <i class="fa fa-circle fa-stack-2x"></i>
                                                      <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="http://twitter.com/">
                                                    <span class="fa-stack fa-lg">
                                                      <i class="fa fa-circle fa-stack-2x"></i>
                                                      <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="http://linkedin.com/">
                                                    <span class="fa-stack fa-lg">
                                                      <i class="fa fa-circle fa-stack-2x"></i>
                                                      <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div><!-- /.profile -->
                                </li><!-- /.team-member -->

                                <li class="team-member">
                                    <img src="{{ asset('public/ezbazzar/images/team/2.jpg') }}" alt="" class="profile-pic img-responsive">
                                    <div class="profile">
                                        <h3>Mahfuzul Alam Shohag<small class="designation">COO</small></h3>
                                        <ul class="social list-unstyled">
                                            <li>
                                                <a href="http://facebook.com/">
                                                    <span class="fa-stack fa-lg">
                                                      <i class="fa fa-circle fa-stack-2x"></i>
                                                      <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="http://twitter.com/">
                                                    <span class="fa-stack fa-lg">
                                                      <i class="fa fa-circle fa-stack-2x"></i>
                                                      <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="http://linkedin.com/">
                                                    <span class="fa-stack fa-lg">
                                                      <i class="fa fa-circle fa-stack-2x"></i>
                                                      <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div><!-- /.profile -->
                                </li><!-- /.team-member -->

                                <li class="team-member">
                                    <img src="{{ asset('public/ezbazzar/images/team/2.jpg') }}" alt="" class="profile-pic img-responsive">
                                    <div class="profile">
                                        <h3>Tanzin Huq Badhon<small class="designation"> CTO</small></h3>
                                        <ul class="social list-unstyled">
                                            <li>
                                                <a href="http://facebook.com/">
                                                    <span class="fa-stack fa-lg">
                                                      <i class="fa fa-circle fa-stack-2x"></i>
                                                      <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="http://twitter.com/">
                                                    <span class="fa-stack fa-lg">
                                                      <i class="fa fa-circle fa-stack-2x"></i>
                                                      <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="http://linkedin.com/">
                                                    <span class="fa-stack fa-lg">
                                                      <i class="fa fa-circle fa-stack-2x"></i>
                                                      <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div><!-- /.profile -->
                                </li><!-- /.team-member -->

                            </ul><!-- /.team-members -->
                        </section><!-- #our-team -->