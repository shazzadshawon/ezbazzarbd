<div class="container-fluid wow fadeInUp">
    <!-- ========================================= CONTENT ========================================= -->
    <div class="col-xs-12 col-md-12 items-holder no-margin">

        <div class="row no-margin cart-item">
            <div class="col-xs-12 col-sm-2 no-margin">
                <a href="#" class="thumb-holder">
                    <img class="lazy" alt="" src="http://placehold.it/73x73" />
                </a>
            </div>

            <div class="col-xs-12 col-sm-5 ">
                <div class="title">
                    <a href="#">white lumia 9001</a>
                </div>
                <div class="brand">sony</div>
            </div>

            <div class="col-xs-12 col-sm-3 no-margin">
                <div class="quantity">
                    <div class="le-quantity">
                        <form>
                            <a class="minus" href="#reduce"></a>
                            <input name="quantity" readonly="readonly" type="text" value="1" />
                            <a class="plus" href="#add"></a>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-2 no-margin">
                <div class="price">
                    &#2547;2000.00
                </div>
                <a class="close-btn" href="#"></a>
            </div>
        </div><!-- /.cart-item -->

        <div class="row no-margin cart-item">
            <div class="col-xs-12 col-sm-2 no-margin">
                <a href="#" class="thumb-holder">
                    <img class="lazy" alt="" src="http://placehold.it/73x73" />
                </a>
            </div>

            <div class="col-xs-12 col-sm-5">
                <div class="title">
                    <a href="#">white lumia 9001 </a>
                </div>
                <div class="brand">sony</div>
            </div>

            <div class="col-xs-12 col-sm-3 no-margin">
                <div class="quantity">
                    <div class="le-quantity">
                        <form>
                            <a class="minus" href="#reduce"></a>
                            <input name="quantity" readonly="readonly" type="text" value="1" />
                            <a class="plus" href="#add"></a>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-2 no-margin">
                <div class="price">
                    &#2547;2000.00
                </div>
                <a class="close-btn" href="#"></a>
            </div>
        </div><!-- /.cart-item -->

        <div class="row no-margin cart-item">
            <div class="col-xs-12 col-sm-2 no-margin">
                <a href="#" class="thumb-holder">
                    <img class="lazy" alt="" src="http://placehold.it/73x73" />
                </a>
            </div>

            <div class="col-xs-12 col-sm-5">
                <div class="title">
                    <a href="#">white lumia 9001 </a>
                </div>
                <div class="brand">sony</div>
            </div>

            <div class="col-xs-12 col-sm-3 no-margin">
                <div class="quantity">
                    <div class="le-quantity">
                        <form>
                            <a class="minus" href="#reduce"></a>
                            <input name="quantity" readonly="readonly" type="text" value="1" />
                            <a class="plus" href="#add"></a>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-2 no-margin">
                <div class="price">
                    &#2547;2000.00
                </div>
                <a class="close-btn" href="#"></a>
            </div>
        </div><!-- /.cart-item -->

        <div class="row no-margin cart-item">
            <div class="col-xs-12 col-sm-2 no-margin">
                <a href="#" class="thumb-holder">
                    <img class="lazy" alt="" src="http://placehold.it/73x73" />
                </a>
            </div>

            <div class="col-xs-12 col-sm-5">
                <div class="title">
                    <a href="#">white lumia 9001 </a>
                </div>
                <div class="brand">sony</div>
            </div>

            <div class="col-xs-12 col-sm-3 no-margin">
                <div class="quantity">
                    <div class="le-quantity">
                        <form>
                            <a class="minus" href="#reduce"></a>
                            <input name="quantity" readonly="readonly" type="text" value="1" />
                            <a class="plus" href="#add"></a>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-2 no-margin">
                <div class="price">
                    &#2547;2000.00
                </div>
                <a class="close-btn" href="#"></a>
            </div>
        </div><!-- /.cart-item -->
    </div>
    <!-- ========================================= CONTENT : END ========================================= -->
</div>

<div class="container-fluid color-bg wow fadeInUp">

    <!-- ========================================= SIDEBAR : END ========================================= -->
</div>