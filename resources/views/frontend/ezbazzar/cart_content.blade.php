


                    @php

                        $i=1;
                        $j=0;
                        $addTocart = DB::table('add_to_carts')->where('session_id', Session::getId())->get();
                    @endphp




                    <section id="your-order">
                        <h2 class="border h1">your Shopping Cart</h2>



                        <table class="table table-wishlist table-responsive cart_summary">
                            <thead>
                            <tr>
                                <th class="cart_product">

                                    Product


                                </th>
                                <th>

                                    Description


                                </th>
                                <th>

                                    Size


                                </th>
                                <th>

                                    Unit price


                                </th>
                                <th>

                                    Qty


                                </th>
                                <th>

                                    Total

                                </th>
                                <th  class="action"><i class="fa fa-trash-o"></i></th>
                            </tr>
                            </thead>
                            <tbody>

                            @php

                                $i=1;
                                $j=0;
                                $addTocart = DB::table('add_to_carts')->where('session_id', Session::getId())->get();
                            @endphp
                            @foreach ($addTocart as $cart)

                                @php
                                    $addTocartImage = DB::table('product_images')->where('product_id', $cart->product_id)->first();
                                @endphp
                                <tr>
                                    <td class="cart_product">
                                        <a href="{{ URL::to('/product-details/'.$cart->product_id) }}"><img style="height: 100px" src="{{asset('product_image/'.$addTocartImage->product_image)}}" alt="Product"></a>
                                    </td>
                                    <td class="cart_description">
                                        <p class="product-name"><a href="{{ URL::to('/product-details/'.$cart->product_id) }}">

                                                {{ $cart->product_name }}


                                            </a></p>
                                        <small class="cart_ref">Item Code : #{{ $cart->product_code }}</small><br>
                                        {{-- <small><a href="#">Color : Beige</a></small><br>
                                        <small><a href="#">Size : S</a></small> --}}
                                    </td>
                                    <td class="price"><span> {{ $cart->size }} </span></td>
                                    <td class="price"><span>
                                          @php

                                              $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
                                              $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
                                              $BnPrice = str_replace($replace_array,$search_array,$cart->product_price);
                                          @endphp


                                            {{ $cart->product_price }} TK


                            </span></td>
                                    <td class="qty">
                                        <h3>
                                            @php

                                                $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
                                                $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
                                                $QTY = str_replace($replace_array,$search_array,$cart->product_quantity);
                                            @endphp


                                            {{ $cart->product_quantity }}


                                        </h3>
                                    </td>
                                    <td class="price">
                                <span>
                                       @php
                                           $sub_total=$cart->product_price*$cart->product_quantity;

                                           $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
                                           $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
                                           $SubPrice = str_replace($replace_array,$search_array,$sub_total);
                                       @endphp


                                    {{ $sub_total }} TK



                                </span>
                                    </td>
                                    <td class="action">
                                        <a href="{{URL::to('/remove-cart-product/'.$cart->id)}}"><b style="font-size: 16px"> X </b></a>
                                    </td>
                                </tr>

                                @php
                                    $j =$j+ $sub_total;
                                    $i++;
                                @endphp
                            @endforeach

                            </tbody>
                            <tfoot>

                            {{-- <tr>
                                <td colspan="3"><strong>Total</strong></td>
                                <td colspan="2"><strong>122.38 €</strong></td>
                            </tr> --}}
                            </tfoot>
                        </table>






                    </section><!-- /#your-order -->

                    <div id="total-area" class="row no-margin">
                        <div class="col-xs-12 col-lg-4 col-lg-offset-8 no-margin-right">
                            <div id="subtotal-holder">
                                <ul class="tabled-data inverse-bold no-border">
                                    <li>
                                        <label>Order total</label>
                                        <div class="value "><h1><strong>&#2547;{{ $j }}</strong></h1></div>
                                    </li>
                                    <li>
                                    <label></label>
                                        <a href="{{ url('shipping') }}" class="btn btn-success btn-lg"> Checkout</a>
                                    </li>

                                </ul><!-- /.tabled-data -->



                            </div><!-- /#subtotal-holder -->
                        </div><!-- /.col -->
                    </div><!-- /#total-area -->
