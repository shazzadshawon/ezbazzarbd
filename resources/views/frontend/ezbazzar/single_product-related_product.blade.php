<section id="recently-reviewd" class="wow fadeInUp">
    <div class="no-container">
        <div class="carousel-holder hover">
            @php
                $products = DB::table('products')->where('sub_category_id',$productInfo->sub_category_id)->get();

            @endphp

            <div class="title-nav">
                <h2 class="h1">Related Products</h2>
                <div class="nav-holder">
                    <a href="#prev" data-target="#owl-recently-viewed-2" class="slider-prev btn-prev fa fa-angle-left"></a>
                    <a href="#next" data-target="#owl-recently-viewed-2" class="slider-next btn-next fa fa-angle-right"></a>
                </div>
            </div><!-- /.title-nav -->

            <div id="owl-recently-viewed-2" class="owl-carousel product-grid-holder">

            @foreach($products as $product)

                <div class=" no-margin carousel-item product-item-holder size-medium hover">
                    <div class="product-item">
                       {{--  <div class="ribbon blue"><span>new!</span></div>
                        <div class="ribbon red"><span>sale</span></div> --}}
                        @php
                            $product_image = DB::table('product_images')->where('product_id',$product->id)->first();
                        @endphp
                        <div class="image">
                            <img alt="" src="{{asset('product_image/'.$product_image->product_image)}}" data-echo="{{asset('product_image/'.$product_image->product_image)}}" style="width: 194px; height: 150px;" />
                        </div>
                        <div class="body">
                            <div class="title">
                                <a href="{{url('product-details/'.$product->id)}}">{{$product->product_name}}</a>
                            </div>

                        </div>
                        <div class="prices">

                            @php
                                $discount_amount = ( $product->product_price * $productInfo->discount / 100 );
                            @endphp
                            <div class="price-current text-right">&#2547;{{ $productInfo->product_price - $discount_amount }}</div>

                        </div>
                        <div class="hover-area">
                            <div class="add-cart-button">
                                <a href="{{url('product-details/'.$product->id)}}" class="le-button">view Details</a>
                            </div>
                            <div class="wish-compare">
                                <a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
                            </div>
                        </div>
                    </div><!-- /.product-item -->
                </div><!-- /.product-item-holder -->
                @endforeach
              </div><!-- /#recently-carousel -->

        </div><!-- /.carousel-holder -->
    </div><!-- /.container -->
</section><!-- /#recently-reviewd -->