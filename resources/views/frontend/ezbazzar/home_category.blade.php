<div id="products-tab" class="wow fadeInUp">
    <div class="container">
        <div class="tab-holder">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" >
                <li class="active"><a href="#featured" data-toggle="tab">featured</a></li>
                <li><a href="#top-sales" data-toggle="tab">top sales</a></li>
                <li><a href="#new-arrivals" data-toggle="tab">new arrivals</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">

                <div class="tab-pane active" id="featured">
                    <div class="product-grid-holder">
                        @foreach($featured as $top)
                            <div class="col-sm-4 col-md-3  no-margin product-item-holder hover">
                                <div class="product-item">

                                    <div class="image">
                                        @php
                                            $product_image = DB::table('product_images')->where('product_id',$top->id)->first();
                                        @endphp
                                        <img style="height: 200px" alt="" src="{{asset('product_image/'.$product_image->product_image)}}" data-echo="{{asset('product_image/'.$product_image->product_image)}}" />
                                    </div>
                                    <div class="body">

                                         @if(!empty( $top->discount ))
                                            <div class="label-discount green">{{ $top->discount  }}% Discount</div>
                                        @else
                                            <div class="label-discount green">0% Discount</div>
                                        @endif
                                        <div class="title">
                                            <a href="{{ url('product-details/'.$top->id) }}">{{$top->product_name}}</a>
                                        </div>
                                        @php
                                            $subcat = DB::table('sub_categories')->where('sub_category_id',$top->sub_category_id)->first();
                                        @endphp
                                        @if(!empty($subcat))
                                        <div class="brand">{{ $subcat->sub_category_name }}</div>
                                        @endif
                                    </div>
                                    <div class="prices">
                                        <div class="price-prev">&#2547;{{ $top->product_price }}</div>
                                        @php
                                            $discount_amount = ( $top->product_price * $top->discount / 100 );
                                        @endphp
                                        <div class="price-current pull-right">&#2547;{{ $top->product_price - $discount_amount }}</div>
                                    </div>

                                    <div class="hover-area">
                                        <div class="add-cart-button">
                                            <a href="{{ url('/product-details/'.$top->id) }}" class="le-button">View Details</a>
                                        </div>
                                       {{--  <div class="wish-compare">
                                            <a class="btn-add-to-wishlist" href="#">Add to wishlist</a>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>


                </div>

                <div class="tab-pane" id="top-sales">
                    <div class="product-grid-holder">
                        @foreach($topsale as $top)
                            <div class="col-sm-4 col-md-3  no-margin product-item-holder hover">
                                <div class="product-item">

                                    <div class="image">
                                        @php
                                            $product_image = DB::table('product_images')->where('product_id',$top->id)->first();
                                        @endphp
                                        <img style="height: 200px" alt="" src="{{asset('product_image/'.$product_image->product_image)}}" data-echo="{{asset('product_image/'.$product_image->product_image)}}" />
                                    </div>
                                    <div class="body">

                                        @if(!empty( $top->discount ))
                                        <div class="label-discount green">{{ $top->discount  }}% Discount</div>
                                        @else
                                            <div class="label-discount green">0% Discount</div>
                                        @endif
                                        <div class="title">
                                            <a href="{{ url('/product-details/'.$top->id) }}">{{$top->product_name}}</a>
                                        </div>
                                        @php
                                            $subcat = DB::table('sub_categories')->where('sub_category_id',$top->sub_category_id)->first();
                                        @endphp
                                        @if(!empty($subcat))
                                        <div class="brand">{{ $subcat->sub_category_name }}</div>
                                        @endif
                                    </div>
                                    <div class="prices">
                                        <div class="price-prev">&#2547;{{ $top->product_price }}</div>
                                        @php
                                            $discount_amount = ( $top->product_price * $top->discount / 100 );
                                        @endphp
                                        <div class="price-current pull-right">&#2547;{{ $top->product_price - $discount_amount }}</div>
                                    </div>

                                    <div class="hover-area">
                                        <div class="add-cart-button">
                                            <a href="{{ url('/product-details/'.$top->id) }}" class="le-button">View Details</a>
                                        </div>
                                        {{-- <div class="wish-compare">
                                            <a class="btn-add-to-wishlist" href="#">Add to wishlist</a>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                            @endforeach

                    </div>


                </div>

                <div class="tab-pane" id="new-arrivals">
                    <div class="product-grid-holder">
                        @foreach($newarrival as $top)
                            <div class="col-sm-4 col-md-3  no-margin product-item-holder hover">
                                <div class="product-item">

                                    <div class="image">
                                        @php
                                            $product_image = DB::table('product_images')->where('product_id',$top->id)->first();
                                        @endphp
                                        <img style="height: 200px" alt="" src="{{asset('product_image/'.$product_image->product_image)}}" data-echo="{{asset('product_image/'.$product_image->product_image)}}" />
                                    </div>
                                    <div class="body">
                                    @if(!empty( $top->discount ))
                                        <div class="label-discount green">{{ $top->discount  }}% Discount</div>
                                    @else
                                        <div class="label-discount green">0% Discount</div>
                                    @endif
                                        <div class="title">
                                            <a href="{{ url('/product-details/'.$top->id) }}">{{$top->product_name}}</a>
                                        </div>
                                        @php
                                            $subcat = DB::table('sub_categories')->where('sub_category_id',$top->sub_category_id)->first();
                                        @endphp
                                        @if(!empty($subcat))
                                        <div class="brand">{{ $subcat->sub_category_name }}</div>
                                        @endif
                                    </div>
                                    <div class="prices">
                                        <div class="price-prev">&#2547;{{ $top->product_price }}</div>
                                        @php
                                            $discount_amount = ( $top->product_price * $top->discount / 100 );
                                        @endphp
                                        <div class="price-current pull-right">&#2547;{{ $top->product_price - $discount_amount }}</div>
                                    </div>

                                    <div class="hover-area">
                                        <div class="add-cart-button">
                                            <a href="{{ url('/product-details/'.$top->id) }}" class="le-button">View Details</a>
                                        </div>
                                       {{--  <div class="wish-compare">
                                            <a class="btn-add-to-wishlist" href="#">Add to wishlist</a>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>