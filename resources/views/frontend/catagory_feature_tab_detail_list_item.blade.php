<li>
    <div class="left-block">
        <a href="detail.php">
        <img class="img-responsive" alt="pants" src="assets/data/mens_fashion/pants/pant2.jpg" /></a>
        <div class="quick-view">
                <a title="Add to my wishlist" class="heart" href="#"></a>
                <a title="Add to compare" class="compare" href="#"></a>
                <a title="Quick view" class="search" href="#"></a>
        </div>
        <div class="add-to-cart">
            <a title="Add to Cart" href="#">Add to Cart</a>
        </div>
    </div>
    <div class="right-block">
        <h5 class="product-name"><a href="detail.php">Pant 02</a></h5>
        <div class="content_price">
            <span class="price product-price">&#2547;3,950</span>
            <span class="price old-price">&#2547;4,950</span>
        </div>
        <div class="product-star">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-half-o"></i>
            <i class="fa fa-star-o"></i>
        </div>
    </div>
</li>