
@php
    $main_menus = \DB::table('categories')->get();
    $mainmenus = \DB::table('categories')->where('mega_menu','!=',1)->take(4)->get();
@endphp


<div id="header" class="header">
    <div class="top-header">
        <div class="container">
            <div class="nav-top-links">
                <a class="first-item" href="#"><img alt="phone" src="{{ asset('assets/images/phone.png') }}" />+8801611392222</a>
            </div>
    {{--         
            <div class="support-link">
                <a href="#">Services</a>
                <a href="#">Support</a>
            </div>
 --}}
            <div id="user-info-top" class="user-info pull-right">
                <div class="dropdown">
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span>My Account</span></a>
                    <ul class="dropdown-menu mega _dropdown" role="menu">
                         @if (Session::has('customer_id'))
                             <li><a href="{{ url('logout') }}">logout ({{ Session::get('customer_name') }})</a></li>
                             <li><a href="{{ url('view-wishlist') }}">Wishlists</a></li>
                         @else
                             <li><a href="{{ asset('User-Register') }}">Login / Register</a></li>
                         @endif
                       
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--/.top-header -->
    <!-- MAIN HEADER -->
    
    <div class="container main-header">
        <div class="row">
            <div class="col-xs-12 col-sm-3 logo">
                <a href="{{ url('/') }}"><img alt="Lakes Point" src="{{ asset('assets/images/lakesLogo_new.png') }}" /></a>
            </div>            
            @include('frontend/header_search')
            @include('frontend/shopping_cart')
        </div>    
    </div>

    <!-- END MAIN HEADER -->
    
    <div id="nav-top-menu" class="nav-top-menu">
        <div class="container">
            <div class="row">
                <div class="col-sm-1" id="box-vertical-megamenus">
                    <div class="box-vertical-megamenus is-home">
                        <h4 class="title">
                            <span class="btn-open-mobile"><i class="fa fa-bars"></i></span>
                        </h4>
                        <div class="vertical-menu-content" style="display: none">
                            <ul class="vertical-menu-list">
                            @foreach($main_menus as $main)
                                <li>
                                    <a class="parent" href="{{ url('Main-Category-products/'. $main->category_id) }}">{{ $main->category_name }}</a>


@php
                                            $subcategories = \DB::table('sub_categories')->where('category_id',$main->category_id)->get();
                                        @endphp
                                        @if(!empty($subcategories))
                                        <div class="vertical-dropdown-menu">
                                        <div class="vertical-groups col-sm-12">
                                            <div class="mega-group col-sm-4">
                                                <ul class="group-link-default">

                                                    @foreach ($subcategories as $sub)
                                                     <li><a href="{{ url('Category-products/'.$sub->sub_category_id) }}">{{ $sub->sub_category_name }}</a></li>
                                                @endforeach
                                                   
                                                </ul>
                                            </div>
                                            <!-- <div class="mega-custom-html col-sm-12">
                                                <a href="#"><img src="assets/data/banner-megamenu.jpg" alt="Banner"></a>
                                            </div> -->
                                        </div>
                                    </div>



                                        @endif




                                    

                                </li>
                            @endforeach

                            </ul>
                            <!-- <div class="all-category"><span class="open-cate">All Categories</span></div> -->
                        </div>
                    </div>
                </div>
                <div id="main-menu" class="col-sm-11 main-menu">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <a class="navbar-brand" href="#">MENU</a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li class="active"> <a href="{{ url('/') }}">Home</a> </li>
                                    <li class="dropdown">
                                        <a href="" class="dropdown-toggle" data-toggle="dropdown">Fashion</a>
                                        <ul class="dropdown-menu mega_dropdown" role="menu" style="width: 830px;">
                                        @php
                                            $megamenus = DB::table('categories')->where('mega_menu',1)->where('publication_status',1)->get();
                                        @endphp
                                        @foreach ($megamenus as $menu)
                                             <li class="block-container col-sm-3">
                                                <ul class="block">
                                                 @php
                                                    $subcategories = \DB::table('sub_categories')->where('category_id',$menu->category_id)->get();
                                                @endphp
                                                    <li class="img_container">
                                                        <a href="#">
                                                            <img class="img-responsive" src="{{ asset('assets/data/men.png') }}" alt="sport">
                                                        </a>
                                                    </li>

                                                    <li class="link_container group_header">
                                                        <a href="{{ url('Main-Category-products/'.$menu->category_id) }}">{{ $menu->category_name }}</a>
                                                    </li>
                                                     @if(!empty($subcategories))
                                                         @foreach ($subcategories as $sub)
                                                          <li class="link_container"><a href="{{ url('Category-products/'.$sub->sub_category_id) }}">{{ $sub->sub_category_name }}</a></li>
                                                         @endforeach
                                                     @endif
 
                                                </ul>
                                             </li>
                                        @endforeach
                                           
                                        </ul>
                                    </li>
                                    @foreach($mainmenus as $main)
                                    <li class="dropdown">
                                        <a href="{{ url('Main-Category-products/'.$main->category_id) }}" class="dropdown-toggle" data-toggle="dropdown">{{ $main->category_name }}</a>
                                        @php
                                            $subcategories = \DB::table('sub_categories')->where('category_id',$main->category_id)->get();
                                        @endphp
                                        @if(!empty($subcategories))
                                        <ul class="dropdown-menu container-fluid">
                                            <li class="block-container">
                                                <ul class="block">
                                                @foreach ($subcategories as $sub)
                                                     <li class="link_container"><a href="{{ url('Category-products/'.$sub->sub_category_id) }}">{{ $sub->sub_category_name }}</a></li>
                                                @endforeach
                                                   
                                                 
                                                </ul>
                                            </li>
                                        </ul> 
                                        @endif
                                    </li>
                                    @endforeach
                                  
                                 
                                </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </nav>
                </div>
            </div>
            <!-- userinfo on top-->
            <div id="form-search-opntop">
            </div>
            <!-- userinfo on top-->
            <div id="user-info-opntop">
            </div>
            <!-- CART ICON ON MMENU -->
            <div id="shopping-cart-box-ontop">
                <i class="fa fa-shopping-cart"></i>
                <div class="shopping-cart-box-ontop-content"></div>
            </div>
        </div>
    </div>
</div>