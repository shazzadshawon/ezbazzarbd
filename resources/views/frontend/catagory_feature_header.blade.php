<div class="collapse navbar-collapse">           
  <ul class="nav navbar-nav">
    <li class="active"><a data-toggle="tab" href="#tab-pants">Pants</a></li>
    <li><a data-toggle="tab" href="#tab-shirts">Shirts</a></li>
    <li><a data-toggle="tab" href="#tab-tshirt">T-Shirts</a></li>
    <li><a data-toggle="tab" href="#tab-poloshirt">Polo Shirts</a></li>
    <li><a data-toggle="tab" href="#tab-watches">Watches</a></li>
    <li><a data-toggle="tab" href="#tab-shoes">Shoes &amp; Sandal</a></li>
  </ul>
</div><!-- /.navbar-collapse -->