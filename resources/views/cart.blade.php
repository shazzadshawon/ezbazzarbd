<!DOCTYPE html>
<html class=""><!--<![endif]-->
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <title>ArtandCreativeinterior</title> 

    <!-- Standard Favicon -->
    <link rel="icon" type="image/x-icon" href="../icon/LOGO.jpg" />
    
    <!-- For iPhone 4 Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images//apple-touch-icon-114x114-precomposed.png">
    
    <!-- For iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images//apple-touch-icon-72x72-precomposed.png">
    
    <!-- For iPhone: -->
    <link rel="apple-touch-icon-precomposed" href="images//apple-touch-icon-57x57-precomposed.png"> 
    
    <!-- Custom - Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('style.css') }}">
    
    <!--[if lt IE 9]>
        <script src="js/html5/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .photo-slider .item {
    padding: 10px 0;
}
.carousel-caption {
  
    padding-bottom: 0px;
}
.middle-header {
    margin-top: 5px;
    margin-bottom: 10px;
}

    </style>
    
</head>

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">
     
    <!-- Header -->
    @include('pages.header')
    <!-- Header /- -->
    
    <main class="site-main page-spacing">
        <!-- Page Banner -->
        <div class="page-banner cart-banner container-fluid no-padding">
            <div class="page-banner-content">
                <h3>Shopping Cart</h3>
            </div>
        </div><!-- Page Banner /- -->
        
        @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3 style="color: green; text-align: center;"> {{Session::get('message')}}</h3>
</div>
      
@endif
        <!-- Cart -->
        <div id="cart-main" class="cart-main">
            <div class="section-padding"></div>
            <div class="container">
                <div class="woocommerce">
                    <form method="post" class="col-md-12 col-sm-12 col-xs-12 no-padding">
                        <table class="shop_table cart">
                            <thead>
                                <tr>
                                    <th class="product-name" colspan="2">Products</th>
                                    <th class="product-price">Price</th>
                                    <th class="product-quantity">Quantity</th>
                                    <th class="product-subtotal">Total</th>
                                    <th class="product-remove">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>

                            @php

                        $i=1;
                        $j=0;
                        $addTocart = DB::table('add_to_carts')->where('session_id', Session::getId())->get(); 
                        
                            @endphp
                        @foreach ($addTocart as $cart)
                           
                           @php
                               $addTocartImage = DB::table('product_images')->where('product_id', $cart->product_id)->first(); 
                           @endphp
                        
                                <tr class="cart_item">
                                    <td data-title="Product" class="product-thumbnail">
                                        <a title="Product Thumbnail" href="">
                                            <img class="attachment-shop_thumbnail wp-post-image" src="{{$addTocartImage->product_image}}" alt="cart" />
                                        </a>                    
                                    </td>
                                    <td class="product-name">
                                        <a title="Product Name" href="#">{{$cart->product_name}}</a>
                                       
                                    </td>
                                    <td data-title="Price" class="product-price">
                                        <span class="amount">{{ $cart->product_price}}TK</span>
                                    </td>
                                    <td data-title="Quantity" class="product-quantity">
                                   <h3> {{$cart->product_quantity}} </h3>
                                        {{-- <div class="quantity">
                                            <input type="button" value="-" class="qtyminus btn" data-field="quantity1" />
                                            <input type="text" name="quantity1" value="0" class="qty btn" />
                                            <input type="button" value="+" class="qtyplus btn" data-field="quantity1" />
                                        </div> --}}
                                    </td>
                                    <td data-title="Total" class="product-subtotal">
                                        <span class="amount">{{$sub_total=$cart->product_price*$cart->product_quantity}}TK</span>
                                    </td>
                                    <td class="product-remove">
                                        <a title="Remove this item" class="remove" href="{{URL::to('/remove-cart-product/'.$cart->product_id)}}"><img src="images/close.png" alt="close" /></a>
                                    </td>
                                </tr>
                                @php
                                     $j =$j+ $sub_total;
                                     $i++; 
                                @endphp
                                @endforeach

                            </tbody>
                        </table>
                    </form>

                    <div class="cart-collaterals row">
                         
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="cart_totals">
                                <h4>Cart Totals</h4>
                                <div class="cart_totals_table">
                                    <table>
                                        <tbody>
                                           
                                            
                                            <tr class="order-total">
                                                <th>Total</th>
                                                <td><strong><span class="amount">{{$j}} TK</span></strong> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>  
                            </div>
                            <div class="wc-proceed-to-checkout">
                                <a title="Proceed To CheckOut" class="btn" href="{{ URL::to('/') }}">Continue Shopping</a>
                                <a title="Proceed To CheckOut" class="btn" href="#">Proceed to Checkout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- container -->
            <div class="section-padding"></div>
        </div><!-- Cart /- -->
    </main>
 @include('pages.footer')
    
    
    
    <!-- JQuery v1.11.3 -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <!-- Library - Js -->
    <script src="{{ asset('libraries/lib.js') }}"></script>
    <!-- Bootstrap JS File v3.3.5 -->
    
    <script src="{{ asset('libraries/jquery.countdown.min.js') }}"></script>
    
    <script src="{{ asset('libraries/lightslider-master/lightslider.js') }}"></script>
    
    <script src="{{ asset('libraries/slick/slick.min.js') }}"></script>

    <!-- Library - Google Map API -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    
    <!-- Library - Theme JS -->
    <script src="{{ asset('js/functions.js') }}"></script>
</body>
</html>