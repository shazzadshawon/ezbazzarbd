@extends('layouts.frontend')

@section('content')



<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        @php
            $ct = DB::table('categories')->where('category_id',$productInfo->category_id)->first();
        @endphp
        <div class="breadcrumb clearfix">
            <a class="home" href="{{ url('/') }}" title="Return to Home">Home</a>
                <span class="navigation-pipe">&nbsp;</span>
            <a href="{{ url('Main-Category-products/'.$ct->category_id) }}" title="Return to Home">{{ $ct->category_name }}</a>
            <span class="navigation-pipe">{{ $productInfo->product_name }}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
    <p class="title_block">CATEGORIES</p>
    <div class="block_content">
        <!-- layered -->
        <div class="layered layered-category">
            <div class="layered-content">
                <ul class="tree-menu">
                @foreach ($categories as $cat)
                     <li>
                        <span></span><a href="{{ url('Main-Category-products/'.$cat->category_id) }}">{{ $cat->category_name }}</a>
                       
                    </li>
                @endforeach
                   
                </ul>
            </div>
        </div>
        <!-- ./layered -->
    </div>
</div>
                

                <!-- ./block category  -->
                <!--./left silde-->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- Product -->
      @php
          $product_images = DB::table('product_images')->where('product_id',$productInfo->id)->get();
      @endphp
                    <div id="product">
                        <div class="primary-box row">
                            <div class="pb-left-column col-xs-12 col-sm-6">
                                <!-- product-imge-->
                                <div class="product-image">
                                    <div class="product-full">
                                        <img id="product-zoom" src='{{ asset('product_image/'.$product_images[0]->product_image) }}' data-zoom-image="{{ asset('product_image/'.$product_images[0]->product_image) }}"/>
                                    </div>
                                    <div class="product-img-thumb" id="gallery_01">
                                        <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="true">
                                        @for($i=0; $i < count($product_images); $i++)
                                            <li>
                                                <a href="#" data-image="{{ asset('product_image/'.$product_images[$i]->product_image) }}" data-zoom-image="{{ asset('product_image/'.$product_images[$i]->product_image) }}">
                                                    <img id="product-zoom"  src="{{ asset('product_image/'.$product_images[$i]->product_image) }}" /> 
                                                </a>
                                            </li>
                                        @endfor
                                           {{--  <li>
                                                <a href="#" data-image="assets/data/mens_fashion/pants/pant3_d_2.jpg" data-zoom-image="assets/data/mens_fashion/pants/pant3_d_2.jpg">
                                                    <img id="product-zoom"  src="assets/data/mens_fashion/pants/pant3_d_2.jpg" /> 
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-image="assets/data/mens_fashion/pants/pant3.jpg" data-zoom-image="assets/data/mens_fashion/pants/pant3.jpg">
                                                    <img id="product-zoom"  src="assets/data/mens_fashion/pants/pant3.jpg" /> 
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-image="assets/data/mens_fashion/pants/pant3_d_1.jpg" data-zoom-image="assets/data/mens_fashion/pants/pant3_d_1.jpg">
                                                    <img id="product-zoom"  src="assets/data/mens_fashion/pants/pant3_d_1.jpg" /> 
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-image="assets/data/mens_fashion/pants/pant3_d_2.jpg" data-zoom-image="assets/data/mens_fashion/pants/pant3_d_2.jpg">
                                                    <img id="product-zoom"  src="assets/data/mens_fashion/pants/pant3_d_2.jpg" /> 
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-image="assets/data/mens_fashion/pants/pant3.jpg" data-zoom-image="assets/data/mens_fashion/pants/pant3.jpg">
                                                    <img id="product-zoom"  src="assets/data/mens_fashion/pants/pant3.jpg" /> 
                                                </a>
                                            </li> --}}
                                        </ul>
                                    </div>
                                </div>
                                <!-- product-imge-->
                            </div>
                            <div class="pb-right-column col-xs-12 col-sm-6">
                                <h1 class="product-name">{{ $productInfo->product_name }}</h1>
                                <div class="product-comments">
                                    <div class="product-star">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>
                                    <div class="comments-advices">
                                        {{-- <a href="#">Based  on 3 ratings</a>
                                        <a href="#"><i class="fa fa-pencil"></i> write a review</a> --}}
                                    </div>
                                </div>
                                <div class="product-price-group">
                                    <span class="price">&#2547;{{ $productInfo->product_price-($productInfo->product_price*$productInfo->discount)/100 }}</span>
                                    <span class="old-price">&#2547;{{ $productInfo->product_price }}</span>
                                    <span class="discount">-{{ $productInfo->discount }}%</span>
                                </div>
                                <div class="info-orther">
                                    <p>Item Code: {{ $productInfo->product_code }}</p>
                                    
                                    <p>Availability: <span class="in-stock">
                                        @if ($productInfo->product_quantity >0)
                                           
                                                 Stock In
                                         
                                        @else
                                          
                                                 Stock Out
                                           
                                        @endif
                                    </span></p>
                                    <p>Condition: New</p>
                                    <?php
                                    
                                    $sizes = DB::table('product_sizes')->where('product_id',$productInfo->id)->get();
                                    
                                    
                                    ?>
                                   {{--  <p>Available Sizes: 
                                    @foreach($sizes as $size)
                                        {{ $size->size }}, 
                                    @endforeach
                                    </p> --}}
                                
                                
                                
                                

                                <div class="form-action">
                                 {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true, 'class'=>'cart']) !!} 
                                 Size :<select name="size">
                                     @foreach ($sizes as $s)
                                         <option value="{{ $s->size }}">{{ $s->size }}</option>
                                     @endforeach
                                 </select>
                                    <div class="button-group">
                                        {{-- <a class="btn-add-cart" href="#">Add to cart</a> --}}

                                          <input type="hidden" name="product_id" value="{{$productInfo->id}}">
                                <input type="hidden" name="product_name" value="{{$productInfo->product_name}}">
                                <input type="hidden" name="product_name_bn" value="{{$productInfo->product_name_bn}}">
                                <input type="hidden" name="product_code" value="{{$productInfo->product_code}}">
                                <input type="hidden" name="product_price" value="@if($productInfo->discount > 0){{ $productInfo->product_price-($productInfo->product_price*$productInfo->discount)/100}}@else{{$productInfo->product_price}}@endif">
                                <input type="hidden" name="product_quantity1" value="1">

                                Quantity :<select name="product_quantity">
                                     @for($i=1; $i<=10; $i++)
                                         <option value="{{ $i }}">{{ $i }}</option>
                                     @endfor
                                 </select>
                                 <br>
                                <input type="hidden" name="publication_status" value="{{$productInfo->publication_status}}">
                                    {{-- <input type="submit" value="ADD TO CART" title=""> --}}
                                   <button class="btn-add-cart" type="submit">
                                       
                                                 Add To Cart
                                            
                                   </button>
                              
                                        {{-- <a class="btn-add-cart" href="#">Add to cart</a> --}}
                                    </div>
                                     {!! Form::close() !!}

                                    </div>

                                </div>


                                    <div class="button-group">
                                          @php
                                 
                                        $wishlist = DB::table('wishlists')
                                                ->where('product_id', $productInfo->id)
                                                ->where('customer_id', Session::get('customer_id'))->first();
                               @endphp
                                          
                                   {{-- wish --}}
                     
                                        @if (Session::has('customer_id'))
                                      
                                        @if($wishlist==NULL)
                                        {!! Form::open(['route' => 'wishlist.store','files'=>true]) !!}
                                         <input type="hidden" name="customer_id" value="{{Session::get('customer_id')}}">
                                        <input type="hidden" name="product_id" value="{{$productInfo->id}}">
                                       <div class="">
                                      
                                        <button type="submit" title="Add to my wishlist" class="fa fa-heart-o" value="W" style="color: green;"></button>
                                        <br>Wishlist
                                     
                                       </div>
                                         {!! Form::close() !!}
                                          @else
                                          <div class="">
                                        <a title="Add to my wishlist" class="fa fa-heart-o" style="color: blue;"><br>Wishlist</a>
                                     
                                             </div>
                                          @endif
                                        @else 
                                        <div class="">
                                        <a title="Add to my wishlist" class="fa fa-heart-o" href="{{URL::to('/User-Register')}}"><br>Wishlist</a>
                                     
                                        </div>
                                   
                                        @endif
{{--
                                        <div class="product-desc">
                                             <span style="font-weight: upper;">Product Details</span>
                                                <p>
                                                     @php
                                                        print_r( $productInfo->description );
                                                    @endphp 
                                                </p>

                                        </div>
--}}
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab product -->
                        <div class="product-tab">
                            <ul class="nav-tab">
                                <li class="active">
                                    <a aria-expanded="false" data-toggle="tab" href="#product-detail">Product Details</a>
                                </li>
                            </ul>
                            <div class="tab-container">
                                <div id="product-detail" class="tab-panel active">
                                     @php
                                        print_r( $productInfo->description );
                                    @endphp 
                                </div>
                            </div>
                        </div> 
                        <!-- ./tab product -->
                        <!-- box product -->

                        <div class="page-product-box">
                            <h3 class="heading">Related Products</h3>
                            <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":3}}'>
                            @php
                                $products = DB::table('products')->where('sub_category_id',$productInfo->sub_category_id)->get();
                            @endphp
                            @foreach ($products as $pro)
                            @php
                               // $p = array($pro);
                            $image = DB::table('product_images')->where('product_id',$pro->id)->first();
                            @endphp
                                 <li>
                                    <div class="product-container">
                                        <div class="left-block">
                                            <a href="{{ asset('product-details/'.$pro->id) }}">
                                                <img style="height: 250px" class="img-responsive" alt="product" src="{{ asset('product_image/'.$image->product_image) }}" />
                                            </a>
                                           {{--  <div class="quick-view">
                                                    <a title="Add to my wishlist" class="heart" href="#"></a>
                                                   
                                            </div> --}}
                                             {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true, 'class'=>'cart']) !!} 
                                    <div class="button-group">
                                        {{-- <a class="btn-add-cart" href="#">Add to cart</a> --}}

                                          <input type="hidden" name="product_id" value="{{$pro->id}}">
                                <input type="hidden" name="product_name" value="{{$pro->product_name}}">
                                <input type="hidden" name="product_name_bn" value="{{$pro->product_name_bn}}">
                                <input type="hidden" name="product_code" value="{{$pro->product_code}}">
                                <input type="hidden" name="product_price" value="@if($pro->discount > 0){{ $pro->product_price-($pro->product_price*$pro->discount)/100}}@else{{$pro->product_price}}@endif">
                                <input type="hidden" name="product_quantity" value="1">
                                <input type="hidden" name="publication_status" value="{{$pro->publication_status}}">
                                    {{-- <input type="submit" value="ADD TO CART" title=""> --}}
                                   <button class="btn-add-cart" type="submit">
                                       
                                                 Add To Cart
                                            
                                   </button>
                              
                                        {{-- <a class="btn-add-cart" href="#">Add to cart</a> --}}
                                    </div>
                                     {!! Form::close() !!}
                                        </div>
                                        <div class="right-block">
                                            <h5 class="product-name"><a href="#">{{ $pro->product_name }}</a></h5>
                                            <div class="product-star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                            </div>
                                            <div class="content_price">
                                                <span class="price product-price">&#2547; {{ $pro->product_price-($pro->product_price*$pro->discount)/100 }}</span>
                                                <span class="price old-price">&#2547;{{ $pro->product_price }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                               
                     
                            </ul>
                        </div>
                        {{-- you might like --}}
                        <div class="page-product-box">
                            <h3 class="heading">Products you might also like</h3>
                            <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":3}}'>
                            @php
                                $products = DB::table('products')->get();
                            @endphp
                            @foreach ($products as $pro)
                            @php
                               // $p = array($pro);
                            $image = DB::table('product_images')->where('product_id',$pro->id)->first();
                            @endphp
                                 <li>
                                    <div class="product-container">
                                        <div class="left-block">
                                            <a href="{{ asset('product-details/'.$pro->id) }}">
                                                <img style="height: 250px" class="img-responsive" alt="product" src="{{ asset('product_image/'.$image->product_image) }}" />
                                            </a>
                                           {{--  <div class="quick-view">
                                                    <a title="Add to my wishlist" class="heart" href="#"></a>
                                                   
                                            </div> --}}
                                             {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true, 'class'=>'cart']) !!} 
                                    <div class="button-group">
                                        {{-- <a class="btn-add-cart" href="#">Add to cart</a> --}}

                                          <input type="hidden" name="product_id" value="{{$pro->id}}">
                                <input type="hidden" name="product_name" value="{{$pro->product_name}}">
                                <input type="hidden" name="product_name_bn" value="{{$pro->product_name_bn}}">
                                <input type="hidden" name="product_code" value="{{$pro->product_code}}">
                                <input type="hidden" name="product_price" value="@if($pro->discount > 0){{ $pro->product_price-($pro->product_price*$pro->discount)/100}}@else{{$pro->product_price}}@endif">
                                <input type="hidden" name="product_quantity" value="1">
                                <input type="hidden" name="publication_status" value="{{$pro->publication_status}}">
                                    {{-- <input type="submit" value="ADD TO CART" title=""> --}}
                                   <button class="btn-add-cart" type="submit">
                                       
                                                 Add To Cart
                                            
                                   </button>
                              
                                        {{-- <a class="btn-add-cart" href="#">Add to cart</a> --}}
                                    </div>
                                     {!! Form::close() !!}
                                        </div>
                                        <div class="right-block">
                                            <h5 class="product-name"><a href="#">{{ $pro->product_name }}</a></h5>
                                            <div class="product-star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                            </div>
                                            <div class="content_price">
                                                <span class="price product-price">&#2547; {{ $pro->product_price-($pro->product_price*$pro->discount)/100 }}</span>
                                                <span class="price old-price">&#2547;{{ $pro->product_price }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                               
                     
                            </ul>
                        </div>
                        <!-- ./box product -->
                    </div>
                <!-- Product -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

@endsection