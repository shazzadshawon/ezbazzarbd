@extends('layouts.backend')
@section('content')
<div class="row">
    <div class=" col-md-10">
        <div class="box-header">
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit Sub category</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
            
        <div class="">
            <div class="">
             	{!! Form::model($sub_category, ['route' => ['sub-category.update',$sub_category->sub_category_id], 'method' => 'PUT', 'name'=>'edit_sub_category', 'class'=>'form-horizontal']) !!}
                <fieldset>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Category</label>
                        <div class="col-md-10">                            
                            <select name="category_id" class="form-control">
                                <option >====Select Category===</option>
                                @foreach ($category as $category_info)
                                <option value="{{$category_info->category_id}}">{{$category_info->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Sub Category Name</label>
                        <div class="col-md-10">
                            <input type="text"  name="sub_category_name" value="{{$sub_category->sub_category_name}}" id="typeahead"  data-provide="typeahead" data-items="4" class="form-control">
                           
                        </div>
                    </div>

                   
                    
                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Publication Status</label>
                        <div class="col-md-10">
                            <select name="publication_status" class="form-control">
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                     <label class="control-label col-md-2" for="typeahead"></label>
                        <div class="col-md-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="reset" class="btn">Cancel</button>
                        </div>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
<script>   
    document.forms['edit_sub_category'].elements['category_id'].value = '{{$sub_category->category_id}}';  
    document.forms['edit_sub_category'].elements['publication_status'].value = '{{$sub_category->publication_status}}';  
</script>
@endsection

