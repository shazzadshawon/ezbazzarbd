@extends('layouts.frontend')

@section('content')
<!-- HEADER -->

<!-- end header -->
{{-- @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h4 style="text-align: center;"> {{Session::get('message')}}</h4>
</div>
      
@endif

 --}}

<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
       
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            
<section id="gaming">
    <div class="grid-list-products">
        <h2 class="section-title">Search Results</h2>


           
        <div class="tab-content">
          
            <div id="list-view" class="products-grid fade tab-pane in active">
                <div class="products-list">
                    
                    @if(!empty($products))
                    @include('frontend/ezbazzar/category_product_list')
                    @else
                    <h3>No Item Found !</h3>
                    @endif
                    
                    

                </div>

            </div><!-- /.products-grid #list-view -->

        </div><!-- /.tab-content -->
    </div><!-- /.grid-list-products -->

</section><!-- /#gaming -->
        </div>
        <!-- ./row-->
    </div>
</div>

@endsection