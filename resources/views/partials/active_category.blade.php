@php
                        $mainCategory = DB::table('categories')->where('publication_status',1)
                        ->get();
@endphp
                                @foreach ($mainCategory as $mainCategoryInfo)

<div class="box-products new-arrivals">
    <div class="container">

        <div class="box-product-head">
            <span class="box-title">
                               @if (Session::has('EN'))
                    {{ $mainCategoryInfo->category_name }}
                                @else
                    {{ $mainCategoryInfo->category_name_bn }}
                                @endif
            
            </span>
            <ul class="box-tabs nav-tab">
                                    @php
                                       $Subcategory = DB::table('sub_categories')
                                       ->where('category_id',$mainCategoryInfo->category_id)
                                       ->where('publication_status',1)
                                       ->get();
                                    @endphp

                                    @foreach ($Subcategory as $SubCategoryInfo)
                                     <li>
                                     <a data-toggle="tab" href="#tab-{{ $SubCategoryInfo->sub_category_id }}">
                                         
                                @if (Session::has('EN'))
                    {{ $SubCategoryInfo->sub_category_name }}
                                @else
                     {{ $SubCategoryInfo->sub_category_name_bn }}
                                @endif

                                     </a>
                                     </li>
                                    @endforeach
                
               
            </ul>
        </div>

        <div class="box-product-content">

            <div class="box-product-adv">
                <ul class="owl-carousel nav-center" data-items="1" data-dots="false" data-autoplay="true" data-loop="true" data-nav="true">
                            @php  
                                 $i=1;
                                 $offer = DB::table('pazzles')->where('publication_status',1)->where('pazzle',$mainCategoryInfo->category_id)->get();
                            @endphp
                            @foreach ($offer as $offerInfo)

                    <li><a href="#"><img src="../{{ $offerInfo->pazzle_image }}" alt="adv" style="height: 348px;"></a></li>
                            @endforeach
                  {{--   <li><a href="#"><img src="assets/data/option4/adv1.jpg" alt="adv"></a></li> --}}
                </ul>
            </div>

            <div class="box-product-list">
                <div class="tab-container">

                     @foreach ($Subcategory as $SubCategoryInfo)
                    <div id="tab-{{ $SubCategoryInfo->sub_category_id }}" class="tab-panel @if($i==1) active @endif">
                        <ul class="product-list owl-carousel nav-center" data-dots="false" data-loop="true" data-nav = "true" data-margin = "10" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":4}}'>
                            
                            @php
                                $products = DB::table('products')->where('publication_status',1)->Where('sub_category_id',$SubCategoryInfo->sub_category_id)->get();
                            @endphp
                            @foreach ($products as $productsInfo)
                                @php
                                    $productsImage = DB::table('product_images')->where('product_id',$productsInfo->id)->first();
                                @endphp
                            <li>
                                <div class="left-block">
                                    <a href="{{ URL::to('/product-details/'.$productsInfo->id) }}"><img class="img-responsive" alt="product" src="../{{ $productsImage->product_image}}" style="height: 250px;" /></a>
                                    <!-------------Wishlist-------------->
                               @php
                                 
                                        $wishlist = DB::table('wishlists')
                                                ->where('product_id', $productsInfo->id)
                                                ->where('customer_id', Session::get('customer_id'))->first();
                               @endphp
                                          
                                   
                     
                                        @if (Session::has('customer_id'))
                                      
                                         @if($wishlist==NULL)
                                        {!! Form::open(['route' => 'wishlist.store','files'=>true]) !!}
                                         <input type="hidden" name="customer_id" value="{{Session::get('customer_id')}}">
                                        <input type="hidden" name="product_id" value="{{$productsInfo->id}}">
                                       <div class="quick-view">
                                      
                                        <input type="submit" title="Add to my wishlist" class="heart" value="W" style="color: green;">
                                     
                                       </div>
                                         {!! Form::close() !!}
                                          @else
                                          <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" style="color: blue;"></a>
                                     
                                             </div>
                                          @endif
                                        @else 
                                        <div class="quick-view">
                                        <a title="Add to my wishlist" class="heart" href="{{URL::to('/User-Register')}}"></a>
                                     
                                        </div>
                                   
                                        @endif
                               
                            <!--------------wishlist--------- -->
                                    {{-- <div class="add-to-cart">
                                        <a title="Add to Cart" href="#">Add to Cart</a>
                                    </div> --}}
                                </div>
                                <div class="right-block">
                                    <h5 class="product-name"><a href="#">
                                            @if (Session::has('EN'))
                                              {{ $productsInfo->product_name }}
                                            @else
                                                {{ $productsInfo->product_name_bn }}
                                            @endif
                                        </a></h5>

                                        @php

    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $bn_number = str_replace($replace_array,$search_array,$productsInfo->product_price);

//     function en2bnNumber ($number){
                                          
//     $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
//     $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
//     $bn_number = str_replace($replace_array,$search_array,$number);

//     return $bn_number;
// }
                                          
                                        @endphp
                                       
                                       @if (Session::has('EN'))


                                             @if ($productsInfo->discount > 0)
                                                 <div class="content_price">
                                            <span class="price product-price">{{ $productsInfo->product_price-($productsInfo->product_price*$productsInfo->discount)/100 }} TK</span>
                                            <span class="price old-price">{{ $productsInfo->product_price }} TK</span>
                                                 </div> 

                                            @else
                                                  <div class="content_price">
                                            <span class="price product-price">{{ $productsInfo->product_price }} TK </span>
                                           
                                                 </div>  
                                            @endif
                                        


                                      @else

                                            @if ($productsInfo->discount > 0)
                                                 <div class="content_price">
                                            <span class="price product-price">
                                            @php
    $bnDis = $productsInfo->product_price-($productsInfo->product_price*$productsInfo->discount)/100;
    $search_array= array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    $replace_array= array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
    $bnDiscount = str_replace($replace_array,$search_array,$bnDis);
                                            @endphp
                                            {{  $bnDiscount}} ট
                                            </span>
                                            <span class="price old-price">{{ $bn_number }} ট</span>
                                                 </div> 

                                            @else
                                                  <div class="content_price">
                                            <span class="price product-price">{{ $bn_number }} ট </span>
                                           
                                                 </div>  
                                            @endif

                                      @endif
                                    
                                </div>
                            </li>
                            @endforeach

                             

                        </ul>
                    </div>
@php
    $i++;
@endphp
@endforeach
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endforeach