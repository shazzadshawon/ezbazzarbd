<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="Shobar Jonno Web">
    <meta name="keywords" content="MediaCenter, Template, eCommerce">
    <meta name="robots" content="all">

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/bootstrap.min.css') }}">

    <!-- Customizable CSS -->
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/green.css') }}">
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/owl.transitions.css') }}">
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/animate.min.css') }}">
    <!--		<link href="public/ezbazzar/css/red.css" rel="alternate stylesheet" title="Red color">-->
    <link rel="stylesheet/less" type="text/css" href="{{ asset('public/ezbazzar/less/green.less') }}" />

    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Icons/Glyphs -->
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/font-awesome.min.css') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('public/ezbazzar/images/favicon_e.ico') }}">

    <!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
    <!--[if lt IE 9]>
    <script src="public/ezbazzar/js/html5shiv.js"></script>
    <script src="public/ezbazzar/js/respond.min.js"></script>
    <![endif]-->

    <script src="{{ asset('public/ezbazzar/js/jquery-1.10.2.min.js') }}"></script>
    <script src="{{ asset('public/ezbazzar/js/jquery-migrate-1.2.1.js') }}"></script>

</head>