<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//
//Route::get('/', function () {
//    return view('pages.home');
//});
Route::group(['middleware'=>['web']],function(){
// Route::get('/admin','AdminController@index');
// Route::post('/dashboard','AdminController@AdminLoginCheck');

// Route::get('/dashboard','AdminMasterController@index');
// Route::get('/logout','AdminMasterController@logout');
Route::get('/search','AjaxController@search');


Route::resource('/subscribe','SubscribeController');
Route::get('/get-data','SubscribeController@gjsons');

Route::get('/get-data','SubscribeController@gjson');

Route::get('/location','HomeController@location');


//----------------------------------------------Start  View ----------------------------
Route::get('/', 'ViewContoller@index');
Route::post('/post_contact', 'ViewContoller@post_contact');
Route::resource('/customer','ViewContoller');
Route::get('product_search','ViewContoller@product_search');
Route::get('/Category-products/{id}', 'ViewContoller@ProductPage');
Route::get('/Main-Category-products/{id}', 'ViewContoller@ProductPageManin');
Route::get('/Sub-Category-products/{id}', 'ViewContoller@ProductPageSub');
Route::get('/product-details/{id}', 'ViewContoller@SingleProductPage');
Route::get('/User-Register', 'ViewContoller@CustomerLogin');
Route::get('/Sign-Up', 'ViewContoller@CustomerSignUp');
Route::post('/customer-login-check', 'ViewContoller@CustomerLoginCheck');
Route::get('/Crazy-Deal/{id}', 'ViewContoller@CrazyDeal');
Route::get('/Offer-Product/{id}', 'ViewContoller@OfferProduct');
Route::get('/logout', 'ViewContoller@logoutcustomer');
Route::get('/about-us', 'ViewContoller@about_us');
Route::get('/contact-us', 'ViewContoller@contact_us');
Route::get('/MD-Message', 'ViewContoller@md_message');
Route::get('/Tream-Condition', 'ViewContoller@treamCondition');
Route::get('/delivery_policy', function()
{
	return view('delivery_policy');
});
Route::get('/privacy', function()
{
	return view('frontend.ezbazzar.privecy');
});
Route::get('/terms', function()
{
	return view('frontend.ezbazzar.terms');
});
Route::get('/refund', function()
{
	return view('frontend.ezbazzar.return');
});
//----------------------------------------------End  View ----------------------------

//----------------------------------------------Start  Wishlist ----------------------------

Route::resource('/wishlist','WishlistController');
Route::get('/view-wishlist','WishlistController@index');
Route::get('/remove-wishlist/{id}','WishlistController@destroy');
Route::get('/cart','ViewContoller@cart');
//----------------------------------------------End  Wishlist ----------------------------

//----------------------------------------------Start  AddToCart ----------------------------
Route::resource('/Add-To-Cart','AddToCartController');
Route::get('/My-Cart','AddToCartController@create');
Route::get('/remove-cart-product/{id}','AddToCartController@destroy');
Route::get('/shipping','AddToCartController@shipping');
//----------------------------------------------End  AddToCart ----------------------------

//----------------------------------------------Start  Order ----------------------------
Route::resource('/Order','OrderController');

//----------------------------------------------End  AddToCart ----------------------------

});

Route::get('searchajax',array('as'=>'searchajax','uses'=>'AjaxController@autoComplete'));

Auth::routes();
$this->get('admin', 'Auth\LoginController@showLoginForm')->name('admin');
$this->post('admin', 'Auth\LoginController@login');


Route::get('/dashboard', 'HomeController@index');
Route::get('/shobarjonnoweb', 'HomeController@index');
Route::get('/subscribers', 'HomeController@subscribers');
Route::get('/deletesubscriber/{id}', 'HomeController@deletesubscriber');
Route::get('/customers', 'HomeController@customers');
Route::get('/deletecustomer/{id}', 'HomeController@deletecustomer');
Route::group(['middleware'=>['auth']],function(){ 
Route::get('/add-admin','HomeController@create');

    Route::get('/editvideo', 'VideoController@edit_video');
    Route::post('/updatevideo/', 'VideoController@update_video');

    Route::get('/edithotdeal', 'HotdealController@edit_hotdeal');
    Route::post('/updatehotdeal', 'HotdealController@update_hotdeal');


$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('add-admin');
$this->post('register', 'Auth\RegisterController@register');
//------------------------------------------------Start category ------------------------------
Route::get('/add-category','CategoryController@create');
Route::resource('/category', 'CategoryController');
Route::get('/manage-category', 'CategoryController@index');
Route::get('/delete-category/{id}', 'CategoryController@destroy');
Route::get('/unpublished-category/{id}', 'CategoryController@unpublished');
Route::get('/published-category/{id}', 'CategoryController@published');
//------------------------------------------------End category ------------------------------

//----------------------------------------------Start Sub Category ----------------------------
Route::get('/add-sub-category','SubCategoryController@create');
Route::resource('/sub-category', 'SubCategoryController');
Route::get('/manage-sub-category','SubCategoryController@index');
Route::get('/delete-sub-category/{id}', 'SubCategoryController@destroy');
Route::get('/unpublished-sub-category/{id}', 'SubCategoryController@unpublished');
Route::get('/published-sub-category/{id}', 'SubCategoryController@published');
//----------------------------------------------End Sub Category ----------------------------

//----------------------------------------------Start sub  Sub Category ----------------------------
Route::get('/add-sub-sub-category','SubSubCategoryController@create');
Route::resource('/sub-sub-category', 'SubSubCategoryController');
Route::get('/manage-sub-sub-category','SubSubCategoryController@index');
Route::get('/delete-sub-sub-category/{id}', 'SubSubCategoryController@destroy');
Route::get('/unpublished-sub-sub-category/{id}', 'SubSubCategoryController@unpublished');
Route::get('/published-sub-sub-category/{id}', 'SubSubCategoryController@published');
//----------------------------------------------End Sub Category ----------------------------

//----------------------------------------------Start Slider Image ----------------------------
Route::get('/add-slider-image','SliderImageController@create');
Route::resource('/slider-image','SliderImageController');
Route::get('/manage-slider-image','SliderImageController@index');
Route::get('/delete-slider-image/{id}', 'SliderImageController@destroy');
Route::get('/unpublished-slider-image/{id}', 'SliderImageController@unpublished');
Route::get('/published-slider-image/{id}', 'SliderImageController@published');
//----------------------------------------------End Slider Image ----------------------------

//----------------------------------------------Add Product ----------------------------
Route::get('/add-product','ProductController@create');
Route::resource('/product','ProductController');
Route::post('/select-sub-categories','AjaxController@SubCategory');
Route::get('/manage-product','ProductController@index');
Route::get('/product_callback','ProductController@product_callback');
Route::get('/delete-product/{id}', 'ProductController@destroy');
Route::get('/unpublished-product/{id}', 'ProductController@unpublished');
Route::get('/published-product/{id}', 'ProductController@published');
Route::get('/delete-product/{product_id}/image/{product_image_id}', 'ProductController@DeleteProductImage');
Route::get('/delete-product/{product_id}/size/{id}', 'ProductController@DeleteProductSize');
Route::post('/update_product_image/{product_id}/{product_image_id}', 'ProductController@UpdateProductImage');
//Route::post('/edit-product-image/{product_image_id}', 'ProductController@UpdateProductImage');
//Route::PUT('/update-product/{product_id}/image/{product_image_id}', 'ProductController@UpdateProductImage');
Route::post('/add-product/{id}/image', 'ProductController@AddProductImage');
Route::post('/add-product/{id}/size', 'ProductController@AddProductSize');
//----------------------------------------------End  Product ----------------------------

Route::get('/manage-order','OrderController@index');
Route::get('/view-order/{id}','OrderController@ViewOrder');
Route::get('/delivered/{id}','OrderController@delivered');
Route::get('/refuse/{id}','OrderController@refuse');
Route::get('/delete-order/{id}','OrderController@destroy');




//-----------------------------------------------------------Offer-----------------------

Route::get('/add-offer','PazzleController@create');

Route::resource('/offer','PazzleController');
Route::get('/manage-offer','PazzleController@index');
Route::get('/setmegaoffer/{id}','PazzleController@setmegaoffer');
Route::get('/delete-offer/{id}', 'PazzleController@destroy');
Route::get('/unpublished-offer/{id}', 'PazzleController@unpublished');
Route::get('/published-offer/{id}', 'PazzleController@published');

Route::get('/subscribe','SubscribeController@store');
Route::get('/show-subscribe','SubscribeController@index');
Route::get('/show-message','SubscribeController@show_message');
Route::get('/delete-subscribe/{id}','SubscribeController@destroy');


			Route::get('/complain', 'ComplainController@complain');
	
		Route::post('/postcomplain', 'ComplainController@postcomplain');








});

// Route::get('admin','')